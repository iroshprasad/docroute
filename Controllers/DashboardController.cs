using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using DocRoute.Models;
using System.Linq;


namespace DocRoute.Controllers
{
    public class DashboardController : Controller
    {
        DocRouteDbContext dbContext = new DocRouteDbContext();
         CommonController commonController = new CommonController();

        public IActionResult Index()
        {
            return View();
        }


        /**
        * Check document tranfered to user
        * *this method use to Check document tranfered to user
        * @param user id
        * @return json array contain status and message
        */
        public IActionResult GetTransferedDocs(String userId)
        {
            try
            {
                Int32 uId = Convert.ToInt32(userId);

                // var _tranDocs = dbContext.Documenttransfer.Where(d => d.TransferToUserId == uId).Join(dbContext.User, d => d.TransferToUserId, u => u.Userid, (d, u) => new
                // {
                //     d,
                //     u
                // }).Join(dbContext.Documentupload, d => d.DocumentId, up => up.DocumentId, (d,up) => new
                // {
                //    d,
                //    up
                // });

                var _tranDocs = from doc in dbContext.Documenttransfer 
                join ut in dbContext.User on doc.TransferToUserId equals ut.Userid
                join docup in dbContext.Documentupload on doc.DocumentId equals docup.DocumentId
                join uf in dbContext.User on doc.TransferFromUserId equals uf.Userid
                where ut.Userid == uId
                select new {
                    doc,ut,uf,docup
                };

                if (_tranDocs.Any())
                {
                    return Json(new { status = true, message = "success", data = _tranDocs });
                }
                else
                {
                    return Json(new { status = true, message = "failed" });
                }
            }
            catch (Exception ex)
            {
                commonController.LogErrors(ex.ToString(),"dashboard");
                return Json(new { status = false, message = "failed" });
            }
        }

    }
}