using System;
using DocRoute.Models;
using Microsoft.AspNetCore.Mvc;



namespace DocRoute.Controllers
{
    public class CommonController : Controller
    {
        private DocRouteDbContext dbContext = new DocRouteDbContext();

        public IActionResult Index()
        {
            return View();
        }
        public void LogErrors(string errorMessage,string page){
            try
            {
                Errorlog errorlog = new Errorlog();
                errorlog.UserId = 0;
                errorlog.ErrorMessage = errorMessage;
                errorlog.ErrorPage = page;
                errorlog.ErrorDate = DateTime.Now.Date;
                errorlog.ErrorTime = DateTime.Now.TimeOfDay;   
 
                dbContext.Errorlog.Add(errorlog);
                int _result = dbContext.SaveChanges();  
 
            }
            catch (Exception ex)
            { 
                ex.ToString();
            }
        }
    }
}