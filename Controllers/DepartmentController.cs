using System;
using DocRoute.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DocRoute.Controllers
{
    public class DepartmentController : Controller
    {
        DocRouteDbContext dbContext = new DocRouteDbContext();
 CommonController commonController = new CommonController();
        public IActionResult Index()
        {
            return View();
        }

        /**
        * Save department
        * *this method use to save department details*
        * @param department model class
        * @return json array contain status and message
        */
        public IActionResult SaveDepartment(Department department){
            try
            {
                dbContext.Department.Add(department);
                int _result = dbContext.SaveChanges(); 

                if (_result == 1)
                {
                    return Json(new { status = true, message = "success"});
                }else{
                    return Json(new { status = true, message = "failed"});
                }
            }
            catch (Exception ex)
            {
                commonController.LogErrors(ex.ToString(),"department");
                return Json(new { status = false, message = "failed"});
            }
        }

        /**
        * Get Department By Name
        * *this method use to get department details by name*
        * @param department model class
        * @return json array contain status and message and department details
        */
        public IActionResult GetDepartmentByName(Department department){
            try
            { 
                var _department = dbContext.Department.Where(x => x.DepartmentName.Equals(department.DepartmentName)).FirstOrDefault();

                //if department exist
                if(_department != null){   
                    return Json(new { status = true, message = "exist",data = _department}); 
                }else{
                    return Json(new { status = true, message = "notexist"});
                }
            }
            catch (Exception ex)
            { commonController.LogErrors(ex.ToString(),"department");
                return Json(new { status = false, message = "failed"});
            }
        } 
        
        /**
        * Update Department
        * *this method use to modify department details
        * @param department model class
        * @return json array contain status and message 
        */
        public IActionResult UpdateDepartment(Department department){
            try
            {
                dbContext.Entry(department).State = EntityState.Modified;  
                dbContext.SaveChanges(); 

                return Json(new { status = true, message = "success"});     
            }
            catch (Exception ex)
            { commonController.LogErrors(ex.ToString(),"department");
                return Json(new { status = false, message = "failed"});
            }
        }

        /**
        * Get All Department
        * *this method use to get all departments details
        * @param 
        * @return json array contain status and message and all departments details
        */
        public IActionResult GetAllDepartments(){
            try
            {
                var _departments = dbContext.Department;

                if(_departments.Any()){
                    return Json(new { status = true, message = "success",data = _departments});
                }else{
                    return Json(new { status = true, message = "failed"});   
                }  
            }
            catch (Exception ex)
            { commonController.LogErrors(ex.ToString(),"department");
                return Json(new { status = false, message = "failed"});
            }
        }

        /**
        * Get Department By Id
        * *this method use to get department details by department id
        * @param department model class
        * @return json array contain status and message and department details
        */
        public IActionResult GetDepartmentById(Department department){
            try
            {
                var _departments = dbContext.Department.Where(x => x.DepartmentId == department.DepartmentId).FirstOrDefault();

                if(_departments != null){
                    return Json(new { status = true, message = "success",data = _departments});
                }else{
                    return Json(new { status = true, message = "failed"});   
                }  
            }
            catch (Exception ex)
            { commonController.LogErrors(ex.ToString(),"department");
                return Json(new { status = false, message = "failed"});
            }
        }

        /**
        * Delete Department
        * *this method use to delete department details by department id
        * @param department model class
        * @return json array contain status and message and department details
        */
        public IActionResult DeleteDepartment(Department department){
            try
            {

                //check if department already assigned
                var _users = dbContext.User.Where(x => x.DepartmentId == department.DepartmentId); 
                if(_users.Any()){
                    return Json(new { status = true, message = "assigned"});
                }

                dbContext.Department.Remove(department);
                int _result = dbContext.SaveChanges();
                

                if(_result == 1){
                    return Json(new { status = true, message = "success"});
                }else{
                    return Json(new { status = true, message = "failed"});   
                }  
            }
            catch (Exception ex)
            { commonController.LogErrors(ex.ToString(),"department");
                return Json(new { status = false, message = "failed"});
            }
        }
    }
}