using System;
using DocRoute.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;

namespace DocRoute.Controllers
{
    public class UserProfileController : Controller
    {
        private DocRouteDbContext dbContext = new DocRouteDbContext();
         CommonController commonController = new CommonController();
        private static string encryptkey = "jiANXize4UtagfkRUgRVLf7y8wmExSZ4";
        public IActionResult Index()
        {
            return View();
        }

        /**
        * get Login user details
        * *this method use to get login user details
        * @param userid
        * @return json array contain status and message and user details
        */
        public IActionResult GetUserDetails(Int32 userId)
        {
            try
            { 
                var _getuser = dbContext.User.Where(u => u.Userid == userId).FirstOrDefault();

                if (_getuser == null)
                {
                    return Json(new { status = true, message = "failed" });
                } 
                return Json(new { status = true, message = "success", data = _getuser });

            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = "failed" });
            }
        }


        /**
        * change current password
        * *this method is use to change current user password*
        */
        public IActionResult ChangePassword(Int32 userId,string currentPassword,string newPassword)
        {
            try
            { 
                var ecryptedcurrentpassword = EncryptProvider.AESEncrypt(currentPassword, encryptkey);

                var _getuser = dbContext.User.Where(u => u.Userid == userId && u.Password == ecryptedcurrentpassword).FirstOrDefault(); 

                if (_getuser != null)
                {
                    var newencryppassword = EncryptProvider.AESEncrypt(newPassword, encryptkey);
                    _getuser.Password = newencryppassword;
                    dbContext.SaveChanges();

                    return Json(new { status = true, message = "success" });
                }else{
                    return Json(new { status = true, message = "nopassword" });
                }
            }
            catch (Exception ex)
            {
                commonController.LogErrors(ex.ToString(),"userprofile");
                return Json(new { status = false, message = "failed" });
            }
        }
    }
}