using System;
using DocRoute.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DocRoute.Controllers
{
    public class UserlevelController : Controller
    {
        private DocRouteDbContext dbContext = new DocRouteDbContext();
 CommonController commonController = new CommonController();
        public IActionResult Index()
        {
            return View();
        }

        /**
        * Save Userlevel
        * *this method use to save Userlevel details*
        * @param Userlevel model class
        * @return json array contain status and message
        */
        public IActionResult SaveUserlevel(Userlevel userlevel){
            try
            {
                dbContext.Userlevel.Add(userlevel);
                int _result = dbContext.SaveChanges(); 

                if (_result == 1)
                {
                    return Json(new { status = true, message = "success"});
                }else{
                    return Json(new { status = true, message = "failed"});
                }
            }
            catch (Exception ex)
            {commonController.LogErrors(ex.ToString(),"userlevel");
                return Json(new { status = false, message = "failed"});
            }
        }

        /**
        * Get Userlevel By Name
        * *this method use to get Userlevel details by name*
        * @param Userlevel model class
        * @return json array contain status and message and Userlevel details
        */
        public IActionResult GetUserlevelByName(Userlevel userlevel){
            try
            { 
                var _Userlevel = dbContext.Userlevel.Where(x => x.UserLevelName.Equals(userlevel.UserLevelName)).FirstOrDefault();

                //if Userlevel exist
                if(_Userlevel != null){   
                    return Json(new { status = true, message = "exist",data = _Userlevel}); 
                }else{
                    return Json(new { status = true, message = "notexist"});
                }
            }
            catch (Exception ex)
            {commonController.LogErrors(ex.ToString(),"userlevel");
                return Json(new { status = false, message = "failed"});
            }
        } 
        
        /**
        * Update Userlevel
        * *this method use to modify Userlevel details
        * @param Userlevel model class
        * @return json array contain status and message 
        */
        public IActionResult UpdateUserlevel(Userlevel userlevel){
            try
            {
                dbContext.Entry(userlevel).State = EntityState.Modified;  
                dbContext.SaveChanges(); 

                return Json(new { status = true, message = "success"});     
            }
            catch (Exception ex)
            {commonController.LogErrors(ex.ToString(),"userlevel");
                return Json(new { status = false, message = "failed"});
            }
        }

        /**
        * Get All Userlevel
        * *this method use to get all Userlevels details
        * @param 
        * @return json array contain status and message and all Userlevels details
        */
        public IActionResult GetAllUserlevels(){
            try
            {
                var _userlevels = dbContext.Userlevel;

                if(_userlevels.Any()){
                    return Json(new { status = true, message = "success",data = _userlevels});
                }else{
                    return Json(new { status = true, message = "failed"});   
                }  
            }
            catch (Exception ex)
            {commonController.LogErrors(ex.ToString(),"userlevel");
                return Json(new { status = false, message = "failed"});
            }
        }

        /**
        * Get Userlevel By Id
        * *this method use to get Userlevel details by Userlevel id
        * @param Userlevel model class
        * @return json array contain status and message and Userlevel details
        */
        public IActionResult GetUserlevelById(Userlevel userlevel){
            try
            {
                var _userlevels = dbContext.Userlevel.Where(x => x.UserLevelId == userlevel.UserLevelId).FirstOrDefault();

                if(_userlevels != null){
                    return Json(new { status = true, message = "success",data = _userlevels});
                }else{
                    return Json(new { status = true, message = "failed"});   
                }  
            }
            catch (Exception)
            {
                return Json(new { status = false, message = "failed"});
            }
        }

        /**
        * Delete Userlevel
        * *this method use to delete Userlevel details by Userlevel id
        * @param Userlevel model class
        * @return json array contain status and message and Userlevel details
        */
        public IActionResult DeleteUserlevel(Userlevel userlevel){
            try
            {
                //check if userlevel already assigned
                var _users = dbContext.User.Where(x => x.UserLevel == userlevel.UserLevelId); 
                if(_users.Any()){
                    return Json(new { status = true, message = "assigned"});
                }

                dbContext.Userlevel.Remove(userlevel);
                int _result = dbContext.SaveChanges();

                if(_result == 1){
                    return Json(new { status = true, message = "success"});
                }else{
                    return Json(new { status = true, message = "failed"});   
                }  
            }
            catch (Exception ex)
            {commonController.LogErrors(ex.ToString(),"userlevel");
                return Json(new { status = false, message = "failed"});
            }
        }
    }
}