using System;
using System.Collections;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text.RegularExpressions;
using DocRoute.Models;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.DocToPDFConverter;
using Syncfusion.Pdf;
using Tesseract;

namespace DocRoute.Controllers
{
    public class UploadHistoryController : Controller
    {
        private DocRouteDbContext dbContext = new DocRouteDbContext();
        CommonController commonController = new CommonController();

        public IActionResult Index()
        {
            return View();
        }

        /**
         * Get All Upload Documents
         * *this method use to get all Upload documents
         * @param 
         * @return json array contain status and message and all Upload Documents 
         */
        public IActionResult GetAllUploadDocuments()
        {
            try
            {
                var _uploaddocuments = dbContext.Documentupload.Where(d => d.Status == 1).Join(dbContext.User, d => d.UserId, u => u.Userid, (d, u) => new
                {
                    d,
                    u
                });

                if (_uploaddocuments.Any())
                {
                    return Json(new { status = true, message = "success", data = _uploaddocuments });
                }
                else
                {
                    return Json(new { status = true, message = "failed" });
                }
            }
            catch (Exception ex)
            {
                commonController.LogErrors(ex.ToString(),"upload");
                return Json(new { status = false, message = "failed" });
            }
        }

        /**
        * *log seach keywords*
        * @param user id and search text 
        */
        public void LogSearchText(Int32 userid,string searchText){
            Searchlog searchlog = new Searchlog();
            searchlog.UserId = userid;
            searchlog.SearchText = searchText;
            searchlog.SearchTime = DateTime.Now.TimeOfDay;
            searchlog.SearchDate = DateTime.Now.Date;

            dbContext.Add(searchlog);
            dbContext.SaveChanges();
        }

        /**
         * Read document
         * *this method use to read document*
         * @param text  
         * @return document/s
         */
        public IActionResult ReadDocument(string userId,string searchText,string documentno)
        {
            try
            { 
                ArrayList foundFiles = new ArrayList();

                if(documentno == null){
                    string filepath = @"wwwroot\documents\";

                    //get files from the all directory
                    string[] filesEntries = Directory.GetFiles(filepath, "*.*", SearchOption.AllDirectories);

                    //save search text
                    LogSearchText(Convert.ToInt32(userId),searchText);

                    //scan pdf for search text and if found bind list of documents to the array list
                    foundFiles = ScanPdf(searchText, filesEntries);

                    if (foundFiles.Count == 0)
                    {
                        return Json(new { status = true, message = "nofound" });
                    }
                }else{
                    //search from the document numbers
                    var _files = dbContext.Documentupload.Where(d=>d.Documentno.Equals(documentno)).ToList();

                    //if found get the document path information
                    if(_files.Count == 0){
                        return Json(new { status = false, message = "failed" });    
                    }else{
                        
                        //bind document path information to the array list
                        string fileDir = Directory.GetCurrentDirectory()+@"\wwwroot\documents";
                        foreach(var file in _files){
                            foundFiles.Add(fileDir+@"\"+file.DocumentId+@"\"+file.DocumentPath);
                        }

                        if (foundFiles.Count == 0)
                        {
                            return Json(new { status = true, message = "nofound" });
                        }
                    }
                }

                string strfoundFiles = string.Join(",", foundFiles.ToArray());
                return Json(new { status = true, message = "success", file = strfoundFiles });

            }
            catch (Exception ex)
            { 
                commonController.LogErrors(ex.ToString(),"upload");
                return Json(new { status = false, message = "failed" });
            }
        }


        /**
         * Scan pdf for seach text 
         * *this method use to search text from pdf
         * @param search text and file directory
         * @return founded files as array list
         */
        private ArrayList ScanPdf(string searchText, string[] filesEntries)
        {
            SautinSoft.PdfFocus f = new SautinSoft.PdfFocus();

            ArrayList foundFiles = new ArrayList();

            //going through each file
            foreach (string fileEntry in filesEntries)
            {
                //file path 
                string fileDir = Path.Combine(Directory.GetCurrentDirectory(), fileEntry);

                if (System.IO.File.Exists(fileDir))
                {
                    //Read PDF to byte array
                    // byte[] pdf = System.IO.File.ReadAllBytes(fileDir);

                    //open pdf file
                    f.OpenPdf(fileDir);

                    //read files for search text
                    if (f.PageCount > 0)
                    {
                        //convert pdf to text file
                        string pdfwords = f.ToText();

                        //Save to text file
                        // string writeto = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\documents\Sample.txt");
                        // System.IO.File.WriteAllText(writeto, pdfwords);

                        if (pdfwords.Trim().Length > 0)
                        {
                            //if file found with search text add to array
                            if (pdfwords.ToLower().Contains(searchText.ToLower()))
                            {
                                foundFiles.Add(fileDir);
                            }
                            else
                            {
                                //if cannot find in pdf then convvert those pdf into images and save same folder 
                                f.ToImage(Path.GetDirectoryName(fileDir), "pageimg");

                                //get image in the folder to search the text
                                string[] imgfilesEntries = Directory.GetFiles(Path.GetDirectoryName(fileDir), "*.png*", SearchOption.AllDirectories);

                                //loop through the images 
                                foreach (string imgfilesEntry in imgfilesEntries)
                                {
                                    string traindatapath = Path.Combine(@"wwwroot", "traineddata");
                                    using (var engine = new TesseractEngine(traindatapath, "eng", EngineMode.Default))
                                    {
                                        using (var img = Pix.LoadFromFile(imgfilesEntry))
                                        {
                                            //extract the text
                                            var page = engine.Process(img);
                                            var imagetext = page.GetText();

                                            //if file found with search text add to array
                                            if (imagetext.ToLower().Contains(searchText.ToLower()))
                                            {
                                                foundFiles.Add(fileDir);
                                            }
                                        }
                                    }
                                    
                                    System.IO.File.Delete(imgfilesEntry);
                                }
                            }
                        }
                    }
                    else
                    {
                        //if current file is word documents then convert to pdf then scan
                        String tempPdf = ScanDocx(fileDir,searchText);

                        if(tempPdf.Trim() != ""){
                            foundFiles.Add(tempPdf);
                        }
                    }
                }
            }
            return foundFiles;
        }


        /**
        * Convert Word to pdf then scan for the search text 
        * *this method use to convert word to pdf then scan for the search text
        * @param word file path , search text directory 
        * @return if found search text then the file path
        */
        private String ScanDocx(string fileDir,string searchText)
        {
            var tempFile = System.IO.Path.GetTempFileName();
            string convertedPDf = Path.GetDirectoryName(tempFile) + @"\converted.pdf";  

            WordDocument wordDocument = new WordDocument(fileDir, FormatType.Docx);
            DocToPDFConverter converter = new DocToPDFConverter();
            PdfDocument pdfDocument = converter.ConvertToPDF(wordDocument);

            converter.Dispose();
            wordDocument.Close();
            pdfDocument.Save(tempFile);
            pdfDocument.Close(true);

            SautinSoft.PdfFocus f = new SautinSoft.PdfFocus();

            ArrayList foundFiles = new ArrayList();

            //open pdf file
            f.OpenPdf(tempFile);

            //read files for search text
            if (f.PageCount > 0)
            {
                //convert pdf to text file
                string pdfwords = f.ToText();

                //Save to text file
                // string writeto = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\documents\Sample.txt");
                // System.IO.File.WriteAllText(writeto, pdfwords);

                if (pdfwords.Trim().Length > 0)
                {
                    //if file found with search text add to array
                    if (pdfwords.ToLower().Contains(searchText.ToLower()))
                    {
                        foundFiles.Add(fileDir);
                    }
                }
            } 
     
            ArrayList files = new ArrayList();
            files.Add(convertedPDf);

            // var tempFile = System.IO.Path.GetTempFileName();
            // System.IO.File.Copy(convertedPDf, tempFile, true); 
            // System.IO.File.Delete(tempFile);

            // System.IO.File.Delete(convertedPDf);

            
            if(foundFiles.Count == 0){
                return "";
            }else{ 
                return fileDir;
            } 
        }

        /**
         * Detect MIME of document
         * *this method use to detect mime type of document*
         * @param base64 string  
         * @return string file extension
         */
        private string detectMimeType(string encoded)
        {
            string result = "";
            Regex regex = new Regex(@"/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/");

            if (encoded == null || encoded == "")
            {
                return result;
            }

            var mime = regex.Match(encoded);

            if (mime.Success)
            {
                result = mime.Value;
            }

            return result;
        }

        /**
         * Save document
         * *this method use to save document*
         * @param document  
         * @return json array contain status and message
         */
        public IActionResult SaveDocument(Documentupload documentupload, string FileExtension)
        {
            try
            {
                //get base64 string
                var base64image = documentupload.DocumentPath;

                //get file extension
                var fileExtension = FileExtension;

                //convert base64 string to bytes
                var bytes = Convert.FromBase64String(base64image.Split("base64,")[1]);

                // full path to file in current project location
                string filedir = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\documents");

                if (!Directory.Exists(filedir))
                { //check if the folder exists;
                    Directory.CreateDirectory(filedir);
                }

                //generate file name
                string filename = Guid.NewGuid().ToString() + "." + fileExtension;
                documentupload.DocumentPath = filename;

                //genearate document no
                string documentno =  GenerateDocumentNo();
                documentupload.Documentno = documentno;

                dbContext.Documentupload.Add(documentupload);
                int result = dbContext.SaveChanges(); 

                //after save record to database save file to local storage
                if (result == 1)
                {
                    //save file
                    string lastInsertId = documentupload.DocumentId.ToString();
                    filedir = Path.Combine(filedir, lastInsertId);

                    if (!Directory.Exists(filedir))
                    { //check if the folder exists;
                        Directory.CreateDirectory(filedir);
                    }

                    string file = Path.Combine(filedir, filename);

                    if (bytes.Length > 0)
                    {
                        using (var stream = new FileStream(file, FileMode.Create))
                        {
                            stream.Write(bytes, 0, bytes.Length);
                            stream.Flush();
                        }
                    }

                    return Json(new { status = true, message = "success", lastInsertId = lastInsertId , documentno = documentno});
                }
                else
                {
                    return Json(new { status = true, message = "failed" });
                }
            }
            catch (Exception ex)
            {
                commonController.LogErrors(ex.ToString(),"upload");
                return Json(new { status = false, message = "failed" });
            }
        }


        /**
        * *this method is to generate document no for uploading document*
        * @return document no
        */
        public string GenerateDocumentNo()
        {    
            string documentno = "";

            var _currentdocumentno = dbContext.Documentno.FirstOrDefault(d=>d.Id.Equals(1));

            if(_currentdocumentno != null){
                documentno = _currentdocumentno.Nextdocumentno.ToString();
                Int64 _currentdocumentnoInt = Convert.ToInt64(_currentdocumentno.Nextdocumentno.ToString()); 
                Int64 nextdocumentno = _currentdocumentnoInt + 1; 
                _currentdocumentno.Nextdocumentno = nextdocumentno.ToString();  
                dbContext.SaveChanges(); 
            } 
            return documentno;
        }
    
    
        /**
         * Transfer document
         * *this method use to transfer document*
         * @param upload history modal class  
         * @return json array contain status and message
         */
         public IActionResult TransferDocument(Documenttransfer documenttransfer){ 
            try
            {
                dbContext.Documenttransfer.Add(documenttransfer);
                int _result = dbContext.SaveChanges(); 

                if (_result == 1)
                {
                    return Json(new { status = true, message = "success"});
                }else{
                    return Json(new { status = true, message = "failed"});
                }
            }
            catch (Exception ex)
            {
                commonController.LogErrors(ex.ToString(),"upload");
                return Json(new { status = false, message = "failed"});
            }
         }


         /**
         * Check document already Transfer to same user
         * *this method use to Check document already Transfer to same user
         * @param document id and user id
         * @return json array contain status and message
         */
         public IActionResult CheckAlreadyTransfer(String userId, String documentId){ 
            try
            {
                Int32 uId = Convert.ToInt32(userId);
                Int32 dId = Convert.ToInt32(documentId);

                var res = dbContext.Documenttransfer.Where(x=>x.TransferToUserId == uId && x.DocumentId == dId).FirstOrDefault();
                
                if (res != null)
                {
                    return Json(new { status = true, message = "success"});
                }else{
                    return Json(new { status = true, message = "failed"});
                }
            }
            catch (Exception ex)
            {
                commonController.LogErrors(ex.ToString(),"upload");
                return Json(new { status = false, message = "failed"});
            }
         }
    }
}