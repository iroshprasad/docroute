using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using DocRoute.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;

namespace DocRoute.Controllers
{
    public class UserController : Controller
    {
        private DocRouteDbContext dbContext = new DocRouteDbContext();
        CommonController commonController = new CommonController();
        private static string encryptkey = "jiANXize4UtagfkRUgRVLf7y8wmExSZ4";

        public IActionResult Index()
        {
            return View();
        }

        /**
        * Save User
        * *this method use to save User details*
        * @param User model class
        * @return json array contain status and message
        */
        public async Task<IActionResult> SaveUser(User user)
        {
            try
            {
                var encryptedPassword =
                    EncryptProvider.AESEncrypt(user.Password, encryptkey);
                dbContext.Entry(user).Property(x => x.Password).CurrentValue =
                    encryptedPassword;
                dbContext
                    .Entry(user)
                    .Property(x => x.ProfileImage)
                    .CurrentValue = "noimage.png";

                string randomnumber =
                    new Random().Next(10000, 99999).ToString();
                var encryptedVerifycode =
                    EncryptProvider.AESEncrypt(randomnumber, encryptkey);

                dbContext.Entry(user).Property(x => x.VerifyCode).CurrentValue =
                    randomnumber;

                dbContext.User.Add(user);
                int _result = dbContext.SaveChanges();

                if (_result == 1)
                {
                    await SendActivation(user.Email,
                    user.Userid,
                    encryptedVerifycode).ConfigureAwait(false);
                    return Json(new
                    {
                        status = true,
                        message = "success",
                        lastInsertId = user.Userid
                    });
                }
                else
                {
                    return Json(new { status = true, message = "failed" });
                }
            }
            catch (Exception ex)
            { commonController.LogErrors(ex.ToString(),"user");
                return Json(new { status = false, message = ex });
            }
        }

        /**
        * Update User
        * *this method use to modify User details
        * @param User model class
        * @return json array contain status and message 
        */
        public async Task<IActionResult> UpdateUser(User user)
        {
            try
            {
                //var aesKey = EncryptProvider.CreateAesKey();
                var _user = dbContext .User .FirstOrDefault(u => u.Userid.Equals(user.Userid));
 
                if (_user == null)
                {
                    return Json(new { status = false, message = "failed" });
                }

                //_user = user; 
                if (user.Password != null)
                    _user.Password = EncryptProvider.AESEncrypt(user.Password, encryptkey); 

                if (_user.DateOfBirth.Year != 1)
                    _user.DateOfBirth = user.DateOfBirth; 

                if ( _user.FirstName != user.FirstName)
                    _user.FirstName = user.FirstName; 

                if ( _user.SecondName != user.SecondName)   
                    _user.SecondName = user.SecondName; 

                if ( _user.UserLevel != user.UserLevel && user.UserLevel != 0)   
                    _user.UserLevel = user.UserLevel; 

                if ( _user.Email != user.Email)   
                    _user.Email = user.Email; 

                if ( _user.Status != user.Status && user.Status != 0)   
                    _user.Status = user.Status; 

                if ( _user.Phoneno != user.Phoneno)   
                    _user.Phoneno = user.Phoneno; 

                if ( _user.DateOfBirth != user.DateOfBirth)   
                    _user.DateOfBirth = user.DateOfBirth; 

                if ( _user.IsDepartmentHead != user.IsDepartmentHead && user.IsDepartmentHead != 0)   
                    _user.IsDepartmentHead = user.IsDepartmentHead; 

                if ( _user.DepartmentId != user.DepartmentId && user.DepartmentId != 0)   
                    _user.DepartmentId = user.DepartmentId; 
                
                dbContext.Update(_user);
                dbContext.SaveChanges();

                return Json(new { status = true, message = "success" });

                //

                // dbContext.Entry(user).State = EntityState.Modified;

                // if (
                //     user.Password == null ||
                //     user.Password == string.Empty ||
                //     user.Password.Trim().Length == 0
                // )
                //     dbContext.Entry(user).Property(x => x.Password).IsModified =
                //         false;
                // else
                // {
                //     var encrypted =
                //         EncryptProvider.AESEncrypt(user.Password, encryptkey);
                //     dbContext
                //         .Entry(user)
                //         .Property(x => x.Password)
                //         .CurrentValue = encrypted;
                // }

                // dbContext.Entry(user).Property(x => x.ProfileImage).IsModified = false;
                // dbContext.Entry(user).Property(x => x.CreatedTime).IsModified = false;
                // dbContext.Entry(user).Property(x => x.CreatedDate).IsModified = false;
                // dbContext.Entry(user).Property(x => x.Status).IsModified = false;

                // if (user.Phoneno == null)
                //     dbContext.Entry(user).Property(x => x.Phoneno).IsModified =
                //         false;

                // dbContext.Entry(user).Property(x => x.Status).IsModified = false;
                // dbContext.Entry(user).Property(x => x.VerifyCode).IsModified = false;

                // if (user.DateOfBirth.Year == 1)
                //     dbContext
                //         .Entry(user)
                //         .Property(x => x.DateOfBirth)
                //         .IsModified = false;

                // dbContext.SaveChanges();

                // return Json(new { status = true, message = "success" });
            }
            catch (Exception ex)
            { commonController.LogErrors(ex.ToString(),"user");
                return Json(new { status = false, message = "failed" });
            }
        }

        /** Send activation email 
        * *this method use send activation email*
        * @prarm email string
        * @return json array contain status and message
        */
        public async Task<bool> SendActivation(string email, Int32 Userid, string verifyCode)
        {
            if (Userid == 0)
            {
                return false;
            }

            var hostesurl =
                "https://localhost:5001/User/EmailActivation?verifyCode=" +
                verifyCode +
                "&userid=" +
                Userid;

            string subject = "Account Activation";
            string body =
                "<br>Click <a href='" +
                hostesurl +
                "'>here</a> to activate your account.";
            SendEmail(email, subject, body);

            return true;
        }

        /**
        * *Activation user account from link*
        * @param verify code and user id from
        * @return json array contain status and message
        */
        public async Task<IActionResult> EmailActivation(string verifyCode, string userid)
        {
            try
            {
                if (
                    verifyCode == null ||
                    verifyCode == "" ||
                    userid == null ||
                    userid == ""
                )
                {
                    return Json(new { status = true, message = "failed" });
                }
                else
                {
                    Int32 convertedUserid = Convert.ToInt32(userid);
                    var decryptedPassword =
                        EncryptProvider.AESDecrypt(verifyCode, encryptkey);

                    var _user =
                        dbContext
                            .User
                            .Where(u =>
                                u.Userid.Equals(convertedUserid) &&
                                u.VerifyCode.Equals(decryptedPassword))
                            .FirstOrDefault();

                    if (_user != null)
                    {
                        _user.Status = 1;
                        _user.VerifyCode = "0";
                        dbContext.SaveChanges();

                        Response.Redirect("../verified/accountverified.html");

                        return Json(new { status = true, message = "success" });
                    }
                    return Json(new { status = true, message = "failed" });
                }
            }
            catch (Exception ex)
            {
                commonController.LogErrors(ex.ToString(),"user");
                return Json(new { status = false, message = ex });
            }
        }

        /** Email function
        * *this function to config email and send*
        * @param to email and email subject and email body
        */
        public void SendEmail(string ToEmail, string Subject, string Body)
        {
            try
            {
                SmtpClient MailClient = new SmtpClient();
                MailMessage MailMsg = new MailMessage();
                MailClient.Host = "smtp.gmail.com";
                MailMsg.To.Add("irosh.prasad1@gmail.com");
                MailMsg.Subject = Subject;
                MailMsg.SubjectEncoding = Encoding.UTF8;
                MailMsg.IsBodyHtml = true;
                MailMsg.From = new MailAddress("wow.crazyum@gmail.com");
                MailMsg.BodyEncoding = Encoding.UTF8;
                MailMsg.Body = Body;
                MailClient.EnableSsl = true;
                MailClient.UseDefaultCredentials = true;
                MailClient.Port = 587;
                MailClient.Credentials =
                    new NetworkCredential("wow.crazyum@gmail.com",
                        "cfmenjyssckiyhry");
                MailClient.Send(MailMsg);
            }
            catch (Exception ex)
            { commonController.LogErrors(ex.ToString(),"user");
            }
        }

        /**
        * Convrt base64 to image and save
        * *this method use to convert base64 to image
        * @param base64 string
        * @return json array contain status and message
        */
        public IActionResult SaveImage(string base64image, string userId, bool isNewUser = true)
        {
            string filedir = "noimage.png";
            string file = "";
            string filename = Guid.NewGuid().ToString() + ".jpg";
            if (base64image != "noimage" && base64image != null)
            {
                var bytes = Convert.FromBase64String(base64image);

                filedir =
                    Path
                        .Combine(Directory.GetCurrentDirectory(),
                        @"wwwroot\img\userimgs\" + userId);

                if (!Directory.Exists(filedir))
                {
                    Directory.CreateDirectory(filedir);
                }

                file = Path.Combine(filedir, filename);

                if (bytes.Length > 0)
                {
                    using (var stream = new FileStream(file, FileMode.Create))
                    {
                        stream.Write(bytes, 0, bytes.Length);
                        stream.Flush();
                    }
                }

                var _user = dbContext.User.FirstOrDefault(u => u.Userid.Equals(Convert.ToInt32(userId)));

                if (_user == null)
                {
                    return Json(new { status = false, message = "failed" });
                }

                _user.ProfileImage = filename;
                dbContext.Update(_user);
                dbContext.SaveChanges();
                // var user = new User() { Userid = Convert.ToInt32(userId), ProfileImage = filename};

                // dbContext.User.Attach(user);
                // dbContext.Entry(user).Property(x => x.ProfileImage).IsModified = true;
                // dbContext.SaveChanges();
 
            }

            if (base64image == "noimage" || base64image == null)
            {
                var _user =
                    dbContext
                        .User
                        .FirstOrDefault(u => u.Userid.Equals(Convert.ToInt32(userId)));

                if (_user == null)
                {
                    return Json(new { status = false, message = "failed" });
                }

                _user.ProfileImage = _user.ProfileImage;
                dbContext.Update(_user);
                dbContext.SaveChanges();
            }

            return Ok();
        }

        /**
        * Get User By Email
        * *this method use to get User details by email*
        * @param User model class
        * @return json array contain status and message and User details
        */
        public IActionResult GetUserByEmail(User user)
        {
            try
            {
                var _user =
                    dbContext
                        .User
                        .Where(x => x.Email.Equals(user.Email))
                        .FirstOrDefault();

                //if User exist
                if (_user != null)
                {
                    return Json(new
                    {
                        status = true,
                        message = "exist",
                        data = _user
                    });
                }
                else
                {
                    return Json(new { status = true, message = "notexist" });
                }
            }
            catch (Exception ex)
            { commonController.LogErrors(ex.ToString(),"user");
                return Json(new { status = false, message = "failed" });
            }
        }

        /**
        * Get All User
        * *this method use to get all Users details
        * @param 
        * @return json array contain status and message and all Users details
        */
        public IActionResult GetAllUsers(Int32 UserId)
        {
            try
            {
                var _users =
                    (
                    from u in dbContext.User
                    join ul
                    in dbContext.Userlevel
                    on u.UserLevel
                    equals ul.UserLevelId
                    join d
                    in dbContext.Department
                    on u.DepartmentId
                    equals d.DepartmentId
                    select
                    new
                    {
                        userid = u.Userid,
                        email = u.Email,
                        userLevel = u.UserLevel,
                        firstName = u.FirstName,
                        secondName = u.SecondName,
                        profileImage =
                            u.ProfileImage == null
                                ? "noimage.png"
                                : u.ProfileImage,
                        departmentId = u.DepartmentId,
                        createdTime = u.CreatedTime,
                        createdDate = u.CreatedDate,
                        status = u.Status,
                        phoneno = u.Phoneno == null ? "" : u.Phoneno,
                        dateOfBirth = u.DateOfBirth,
                        isDepartmentHead = u.IsDepartmentHead,
                        departmentName = d.DepartmentName,
                        userLevelName = ul.UserLevelName
                    }
                    ).Where(u => u.userid != UserId).ToList();

                if (_users.Any())
                {
                    return Json(new
                    {
                        status = true,
                        message = "success",
                        data = _users
                    });
                }
                else
                {
                    return Json(new { status = true, message = "failed" });
                }
            }
            catch (Exception ex)
            { commonController.LogErrors(ex.ToString(),"user");
                return Json(new { status = false, message = "failed" });
            }
        }

        /**
        * Get User By Id
        * *this method use to get User details by User id
        * @param User model class
        * @return json array contain status and message and User details
        */
        public IActionResult GetUserById(User user)
        {
            try
            {
                var _Users =
                    dbContext
                        .User
                        .Where(x => x.Userid == user.Userid)
                        .FirstOrDefault();

                if (_Users != null)
                {
                    return Json(new
                    {
                        status = true,
                        message = "success",
                        data = _Users
                    });
                }
                else
                {
                    return Json(new { status = true, message = "failed" });
                }
            }
            catch (Exception ex)
            { commonController.LogErrors(ex.ToString(),"user");
                return Json(new { status = false, message = "failed" });
            }
        }

        /**
        * Delete User
        * *this method use to delete User details by User id
        * @param User model class
        * @return json array contain status and message and User details
        */
        public IActionResult DeleteUser(User user)
        {
            try
            {
                dbContext.User.Remove(user);
                int _result = dbContext.SaveChanges();

                if (_result == 1)
                {
                    return Json(new { status = true, message = "success" });
                }
                else
                {
                    return Json(new { status = true, message = "failed" });
                }
            }
            catch (Exception ex)
            { commonController.LogErrors(ex.ToString(),"user");
                return Json(new { status = false, message = "failed" });
            }
        }
    }
}
