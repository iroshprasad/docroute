using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DocRoute.Models;
using System.Text.Encodings.Web;
using NETCore.Encrypt;

namespace DocRoute.Controllers
{
    public class LoginController : Controller
    {
        DocRouteDbContext dbContext = new DocRouteDbContext();
 CommonController commonController = new CommonController();
        private static string encryptkey = "jiANXize4UtagfkRUgRVLf7y8wmExSZ4";
        
        public IActionResult Index()
        {
            return View();
        }


        /**
        * Validate Login user
        * *this method use to validate login user
        * @param 
        * @return json array contain status and message and user details
        */
        public IActionResult UserValidate(User user){
            try
            {
                var encrypted  = EncryptProvider.AESEncrypt(user.Password, encryptkey);

                var _getuser = dbContext.User.Where(u=>u.Email == user.Email && u.Password == encrypted && u.Status == 1).Select(u=>new{
                    u.Userid,
                    u.Email,
                    u.FirstName,
                    u.SecondName,
                    u.UserLevel,
                    u.ProfileImage
                }).FirstOrDefault();
                            
                if(_getuser != null){ 

                    /**update login history**/   
                    Int32 userId = _getuser.Userid;
                    UpdateLoginHistory(userId);

                    // foreach(User u in _getuser){
                    //     TempData["userId"] = u.Userid;
                    //     TempData["firstName"] = u.FirstName;
                    //     TempData["lastName"] = u.SecondName;
                    //     TempData["userLevel"] = u.UserLevel;
                    //     TempData["profileImage"] = "/img/userimgs/"+u.Userid+"/"+u.ProfileImage;
                    // } 

                    return Json(new { status = true, message = "success",data = _getuser});
                }else{
                    return Json(new { status = true, message = "failed"});
                }
            }
            catch (Exception ex)
            {commonController.LogErrors(ex.ToString(),"login");
                return Json(new { status = false, message = "failed" });
            }  
        }

        /**
        * Validate Login user
        * *this method use to validate login user
        * @param 
        * @return json array contain status and message and user details
        */
        private void UpdateLoginHistory(Int32 userId){
            try
            {
                Loginhistory loginhistory = new Loginhistory();
                loginhistory.UserId = userId;
                loginhistory.LoginDate = DateTime.Now.Date;
                loginhistory.LoginTime = DateTime.Now.TimeOfDay;   
 
                dbContext.Loginhistory.Add(loginhistory);
                int _result = dbContext.SaveChanges();  
 
            }
            catch (Exception ex)
            { commonController.LogErrors(ex.ToString(),"login");

                ex.ToString();
            }
        }
    }
}