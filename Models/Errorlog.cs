﻿using System;
using System.Collections.Generic;

namespace DocRoute.Models
{
    public partial class Errorlog
    {
        public int ErrorId { get; set; }
        public int? UserId { get; set; }
        public string ErrorMessage { get; set; }
        public TimeSpan ErrorTime { get; set; }
        public DateTime ErrorDate { get; set; }
        public string ErrorPage { get; set; }
    }
}
