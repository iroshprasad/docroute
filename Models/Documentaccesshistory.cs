﻿using System;
using System.Collections.Generic;

namespace DocRoute.Models
{
    public partial class Documentaccesshistory
    {
        public int AccessHistoryId { get; set; }
        public int DocumentId { get; set; }
        public int UserId { get; set; }
        public DateTime AccessDate { get; set; }
        public TimeSpan AccessTime { get; set; }
        public int AccessType { get; set; }
    }
}
