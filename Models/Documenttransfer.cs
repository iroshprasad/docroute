﻿using System;
using System.Collections.Generic;

namespace DocRoute.Models
{
    public partial class Documenttransfer
    {
        public int TransferId { get; set; }
        public int DocumentId { get; set; }
        public DateTime? TransferDate { get; set; }
        public TimeSpan? TransferTime { get; set; }
        public int? TransferToUserId { get; set; }
        public int? TransferFromUserId { get; set; }
        public string Remark { get; set; }
    }
}
