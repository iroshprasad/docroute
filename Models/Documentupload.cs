﻿using System;
using System.Collections.Generic;

namespace DocRoute.Models
{
    public partial class Documentupload
    {
        public int DocumentId { get; set; }
        public string Documentno { get; set; }
        public int UserId { get; set; }
        public string Remark { get; set; }
        public int Status { get; set; }
        public DateTime UploadDate { get; set; }
        public TimeSpan UploadTime { get; set; }
        public string DocumentPath { get; set; }
    }
}
