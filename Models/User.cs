﻿using System;
using System.Collections.Generic;

namespace DocRoute.Models
{
    public partial class User
    {
        public int Userid { get; set; }
        public string Email { get; set; }
        public int UserLevel { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string ProfileImage { get; set; }
        public int DepartmentId { get; set; }
        public string Password { get; set; }
        public DateTime CreatedDate { get; set; }
        public TimeSpan CreatedTime { get; set; }
        public string VerifyCode { get; set; }
        public int Status { get; set; }
        public string Phoneno { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int IsDepartmentHead { get; set; }
    }
}
