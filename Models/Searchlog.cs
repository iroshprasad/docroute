﻿using System;
using System.Collections.Generic;

namespace DocRoute.Models
{
    public partial class Searchlog
    {
        public int SearchId { get; set; }
        public int UserId { get; set; }
        public string SearchText { get; set; }
        public DateTime SearchDate { get; set; }
        public TimeSpan SearchTime { get; set; }
    }
}
