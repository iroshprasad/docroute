﻿using System;
using System.Collections.Generic;

namespace DocRoute.Models
{
    public partial class Loginhistory
    {
        public int LoginHistoryId { get; set; }
        public int UserId { get; set; }
        public DateTime LoginDate { get; set; }
        public TimeSpan LoginTime { get; set; }
        public DateTime LogoutDate { get; set; }
        public TimeSpan LogoutTime { get; set; }
    }
}
