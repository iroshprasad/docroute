﻿using System;
using System.Collections.Generic;

namespace DocRoute.Models
{
    public partial class Userlevel
    {
        public int UserLevelId { get; set; }
        public string UserLevelName { get; set; }
        public int Status { get; set; }
    }
}
