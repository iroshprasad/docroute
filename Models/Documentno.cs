﻿using System;
using System.Collections.Generic;

namespace DocRoute.Models
{
    public partial class Documentno
    {
        public int Id { get; set; }
        public string Nextdocumentno { get; set; }
    }
}
