﻿using System;
using System.Collections.Generic;

namespace DocRoute.Models
{
    public partial class Department
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int Status { get; set; }
    }
}
