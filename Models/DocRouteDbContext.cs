﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DocRoute.Models
{
    public partial class DocRouteDbContext : DbContext
    {
        public DocRouteDbContext()
        {
        }

        public DocRouteDbContext(DbContextOptions<DocRouteDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<Documentaccesshistory> Documentaccesshistory { get; set; }
        public virtual DbSet<Documentno> Documentno { get; set; }
        public virtual DbSet<Documenttransfer> Documenttransfer { get; set; }
        public virtual DbSet<Documentupload> Documentupload { get; set; }
        public virtual DbSet<Errorlog> Errorlog { get; set; }
        public virtual DbSet<Loginhistory> Loginhistory { get; set; }
        public virtual DbSet<Searchlog> Searchlog { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Userlevel> Userlevel { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=localhost;user id=root;password=root;database=docroute", x => x.ServerVersion("10.1.37-mariadb"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Department>(entity =>
            {
                entity.ToTable("department");

                entity.Property(e => e.DepartmentId)
                    .HasColumnName("departmentId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DepartmentName)
                    .IsRequired()
                    .HasColumnName("departmentName")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Documentaccesshistory>(entity =>
            {
                entity.HasKey(e => e.AccessHistoryId)
                    .HasName("PRIMARY");

                entity.ToTable("documentaccesshistory");

                entity.Property(e => e.AccessHistoryId)
                    .HasColumnName("accessHistoryId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.AccessDate)
                    .HasColumnName("accessDate")
                    .HasColumnType("date");

                entity.Property(e => e.AccessTime)
                    .HasColumnName("accessTime")
                    .HasColumnType("time");

                entity.Property(e => e.AccessType)
                    .HasColumnName("accessType")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DocumentId)
                    .HasColumnName("documentId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("userId")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Documentno>(entity =>
            {
                entity.ToTable("documentno");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Nextdocumentno)
                    .HasColumnName("nextdocumentno")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<Documenttransfer>(entity =>
            {
                entity.HasKey(e => e.TransferId)
                    .HasName("PRIMARY");

                entity.ToTable("documenttransfer");

                entity.Property(e => e.TransferId)
                    .HasColumnName("transferId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DocumentId)
                    .HasColumnName("documentId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Remark)
                    .HasColumnName("remark")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TransferDate)
                    .HasColumnName("transferDate")
                    .HasColumnType("date");

                entity.Property(e => e.TransferFromUserId)
                    .HasColumnName("transferFromUserId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TransferTime)
                    .HasColumnName("transferTime")
                    .HasColumnType("time");

                entity.Property(e => e.TransferToUserId)
                    .HasColumnName("transferToUserId")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Documentupload>(entity =>
            {
                entity.HasKey(e => e.DocumentId)
                    .HasName("PRIMARY");

                entity.ToTable("documentupload");

                entity.Property(e => e.DocumentId)
                    .HasColumnName("documentId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DocumentPath)
                    .IsRequired()
                    .HasColumnName("documentPath")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Documentno)
                    .HasColumnName("documentno")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Remark)
                    .HasColumnName("remark")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UploadDate)
                    .HasColumnName("uploadDate")
                    .HasColumnType("date");

                entity.Property(e => e.UploadTime)
                    .HasColumnName("uploadTime")
                    .HasColumnType("time");

                entity.Property(e => e.UserId)
                    .HasColumnName("userId")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Errorlog>(entity =>
            {
                entity.HasKey(e => e.ErrorId)
                    .HasName("PRIMARY");

                entity.ToTable("errorlog");

                entity.Property(e => e.ErrorId)
                    .HasColumnName("errorId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ErrorDate)
                    .HasColumnName("errorDate")
                    .HasColumnType("date");

                entity.Property(e => e.ErrorMessage)
                    .IsRequired()
                    .HasColumnName("errorMessage")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ErrorPage)
                    .IsRequired()
                    .HasColumnName("errorPage")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ErrorTime)
                    .HasColumnName("errorTime")
                    .HasColumnType("time");

                entity.Property(e => e.UserId)
                    .HasColumnName("userId")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Loginhistory>(entity =>
            {
                entity.ToTable("loginhistory");

                entity.Property(e => e.LoginHistoryId)
                    .HasColumnName("loginHistoryId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.LoginDate)
                    .HasColumnName("loginDate")
                    .HasColumnType("date");

                entity.Property(e => e.LoginTime)
                    .HasColumnName("loginTime")
                    .HasColumnType("time");

                entity.Property(e => e.LogoutDate)
                    .HasColumnName("logoutDate")
                    .HasColumnType("date");

                entity.Property(e => e.LogoutTime)
                    .HasColumnName("logoutTime")
                    .HasColumnType("time");

                entity.Property(e => e.UserId)
                    .HasColumnName("userId")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Searchlog>(entity =>
            {
                entity.HasKey(e => e.SearchId)
                    .HasName("PRIMARY");

                entity.ToTable("searchlog");

                entity.Property(e => e.SearchId)
                    .HasColumnName("searchId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SearchDate)
                    .HasColumnName("searchDate")
                    .HasColumnType("date");

                entity.Property(e => e.SearchText)
                    .IsRequired()
                    .HasColumnName("searchText")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.SearchTime)
                    .HasColumnName("searchTime")
                    .HasColumnType("time");

                entity.Property(e => e.UserId)
                    .HasColumnName("userId")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.Property(e => e.Userid)
                    .HasColumnName("userid")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("createdDate")
                    .HasColumnType("date");

                entity.Property(e => e.CreatedTime)
                    .HasColumnName("createdTime")
                    .HasColumnType("time");

                entity.Property(e => e.DateOfBirth)
                    .HasColumnName("dateOfBirth")
                    .HasColumnType("date");

                entity.Property(e => e.DepartmentId)
                    .HasColumnName("departmentId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varchar(100)")
                    .HasDefaultValueSql("''")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnName("firstName")
                    .HasColumnType("varchar(100)")
                    .HasDefaultValueSql("''")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.IsDepartmentHead)
                    .HasColumnName("isDepartmentHead")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasColumnType("varchar(250)")
                    .HasDefaultValueSql("''")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Phoneno)
                    .IsRequired()
                    .HasColumnName("phoneno")
                    .HasColumnType("varchar(50)")
                    .HasDefaultValueSql("''")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ProfileImage)
                    .IsRequired()
                    .HasColumnName("profileImage")
                    .HasColumnType("varchar(100)")
                    .HasDefaultValueSql("''")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.SecondName)
                    .IsRequired()
                    .HasColumnName("secondName")
                    .HasColumnType("varchar(100)")
                    .HasDefaultValueSql("''")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserLevel)
                    .HasColumnName("userLevel")
                    .HasColumnType("int(11)");

                entity.Property(e => e.VerifyCode)
                    .IsRequired()
                    .HasColumnName("verifyCode")
                    .HasColumnType("varchar(50)")
                    .HasDefaultValueSql("''")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<Userlevel>(entity =>
            {
                entity.ToTable("userlevel");

                entity.Property(e => e.UserLevelId)
                    .HasColumnName("userLevelId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserLevelName)
                    .IsRequired()
                    .HasColumnName("userLevelName")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
