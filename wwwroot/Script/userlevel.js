//on page load
$(document).ready(function() {
  $("#btnCancelUserlevel").trigger("click");
  loadUserlevel();
});

//on add Userlevel click
$("#btnModalUserlevel").on("click", function() {
  $("#modalUserlevel").modal("show");
  $("#btnCancelUserlevel").trigger("click");
});
 

//on modal popup Userlevel cancel click
$("#btnCancelUserlevel").on("click", function() {
  $("#inputUserlevel").val("");
  $("#inputStatus").prop("checked", true);
  $("#inputHiddenUserlevelId").val("");
});

//on modal save Userlevel save click
$("#btnSaveUserlevel").on("click", function() {
  //check already exists
  getUserlevelByName();
});

//function save Userlevel
function saveUserlevel(inputUserlevel, inputStatus) {
  $.ajax({
    type: "POST",
    url: "Userlevel/SaveUserlevel",
    data: {
      UserLevelName: inputUserlevel,
      Status: inputStatus
    },
    error: function(result) {
      Swal.fire("Save unsuccess!", "", "error");
    },
    success: function(result) {
      if (result.status == true && result.message == "success") {
        Swal.fire("Save success!", "", "success");
        loadUserlevel();
        $("#btnCancelUserlevel").trigger("click");
      } else {
        Swal.fire("Save unsuccess!", "", "error");
      }
    }
  });
}

//function update Userlevel
function updateUserlevel(hidUserlevelId, inputUserlevel, inputStatus) {
  $.ajax({
    type: "POST",
    url: "Userlevel/UpdateUserlevel",
    data: {
      UserlevelId: hidUserlevelId,
      UserLevelName: inputUserlevel,
      Status: inputStatus
    },
    error: function(result) {
      Swal.fire("Save unsuccess!", "", "error");
    },
    success: function(result) {
      if (result.status == true && result.message == "success") {
        Swal.fire("Save success!", "", "success");
        loadUserlevel();
        $("#btnCancelUserlevel").trigger("click");
      } else {
        Swal.fire("Save unsuccess!", "", "error");
      }
    }
  });
}


//function get Userlevel by name
function getUserlevelByName() {
  var inputUserlevel = $("#inputUserlevel").val();
  var inputStatus = $("#inputStatus").prop("checked") == true ? 1 : 0;
  var hidUserlevelId =
    $("#inputHiddenUserlevelId").val() == ""
      ? "0"
      : $("#inputHiddenUserlevelId").val();

  if (inputUserlevel.length <= 0 || inputUserlevel.trim() == "") {
    Swal.fire("Fill the required!", "", "warning");
    return;
  }

  $.ajax({
    type: "POST",
    url: "Userlevel/GetUserlevelByName",
    data: {
      UserLevelName: inputUserlevel 
    },
    error: function(result) {
      Swal.fire("Save unsuccess!", "", "error");
    },
    success: function(result) {
      console.log(hidUserlevelId);
      pleaseWait();

      if (result.status == true) {
        //if modify exist Userlevel
        if (
          (result.message == "exist" &&
            hidUserlevelId == result.data.userLevelId) ||
          (result.message == "notexist" && hidUserlevelId != "0")
        ) {
          updateUserlevel(hidUserlevelId, inputUserlevel, inputStatus);
        } else if (result.message == "notexist" && hidUserlevelId == "0") {
          saveUserlevel(inputUserlevel, inputStatus);
        } else {
          Swal.fire("Userlevel already exists!", "", "error");
        }
      }

      if (result.status == false && result.message == "failed") {
        Swal.fire("Save unsuccess!", "", "error");
      }
    }
  });
}

//function load Userlevels
function loadUserlevel() {
  $.ajax({
    type: "POST",
    url: "Userlevel/GetAllUserlevels",
    error: function(result) {
      console.log(result);
    },
    success: function(result) {
      console.log(result);

      if (result.status == true && result.message == "success") {
        var tableBodyUserlevel = "";

        $("#tableBodyUserlevel").html("");

        for (i in result.data) {
          tableBodyUserlevel += '<tr id="' + result.data[i].userLevelId + '">';
          tableBodyUserlevel += "<td>" + result.data[i].userLevelName + "</td>";

          if (result.data[i].status == 1) {
            tableBodyUserlevel +=
              '<td><span class="badge badge-dot mr-4"><i class="bg-success"></i> active</span></td>';
          } else {
            tableBodyUserlevel +=
              '<td><span class="badge badge-dot mr-4"><i class="bg-danger"></i> inactive</span></td>';
          }

          tableBodyUserlevel += "<td>";
          tableBodyUserlevel +=
            '<a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
          tableBodyUserlevel += '<i class="fas fa-ellipsis-v"></i>';
          tableBodyUserlevel += "</a>";
          tableBodyUserlevel +=
            '<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">';
          tableBodyUserlevel +=
            '<a class="dropdown-item viewUserlevel" href="#"><i class="ni ni-ruler-pencil"></i> View</a>';
          tableBodyUserlevel +=
            '<a class="dropdown-item deleteUserlevel" href="#"><i class="ni ni-fat-remove"></i> Delete</a>';
          tableBodyUserlevel += "</div>";
          tableBodyUserlevel += "</td>";
          tableBodyUserlevel += "</tr>";
        }

        $("#tableBodyUserlevel").html(tableBodyUserlevel);
      }
    }
  });
}

//on Userlevel table row delete click
$("#tableBodyUserlevel").on("click", ".deleteUserlevel", function() {
  var userlevelId = $(this)
    .closest("tr")
    .attr("id");

  Swal.fire({
    title: "Are you sure?",
    text: "You won't be able to revert this!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Yes"
  }).then(result => {
    if (result.value) {
      pleaseWait();

      $.ajax({
        type: "POST",
        url: "Userlevel/DeleteUserlevel",
        data: {
          UserlevelId: userlevelId
        },
        error: function(result) {
          Swal.fire("Delete unsuccess!", "", "error");
        },
        success: function(result) {
          console.log(result);

          if (result.status == true && result.message == "assigned") {
            Swal.fire("Delete unsuccess!", "Userlevel already assigned to users", "success");
            return;
          }

          if (result.status == true && result.message == "success") {
            Swal.fire("Delete success!", "", "success");

            loadUserlevel();
          } else {
            Swal.fire("Delete unsuccess!", "", "error");
          }
        }
      });
    }
  });
});

//on Userlevel table row view click
$("#tableBodyUserlevel").on("click", ".viewUserlevel", function() {
  var userlevelId = $(this)
    .closest("tr")
    .attr("id");

  $.ajax({
    type: "POST",
    url: "Userlevel/GetUserlevelById",
    data: {
      UserlevelId: userlevelId
    },
    error: function(result) {
      console.log(result);
    },
    success: function(result) {
      console.log(result);
      if (result.status == true && result.message == "success") {
        $("#btnCancelUserlevel").trigger("click");

        $("#modalUserlevel").modal("show");

        $("#inputUserlevel").val(result.data.userLevelName);
        $("#inputStatus").prop(
          "checked",
          result.data.status == 1 ? true : false
        );
        $("#inputHiddenUserlevelId").val(result.data.userLevelId);
      }
    }
  });
});
