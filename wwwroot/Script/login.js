//validate user login
function UserValidate() {
    $.ajax({
        type: "POST",
        url: "Login/UserValidate",
        data: {
            Email: $("#email").val(),
            Password: $("#password").val()
        },
        error: function(result) {
            console.log(result);
            Swal.fire("Invalid Login!", "", "error");
        },
        success: function(result) {
            console.log(result);
            if (result.status == true && result.message == "success") {

                for (u in result.data) {
                    sessionStorage.setItem("Userid",result.data.userid);
                    sessionStorage.setItem("Email",result.data.email);
                    sessionStorage.setItem("FirstName",result.data.firstName);
                    sessionStorage.setItem("SecondName",result.data.secondName);
                    sessionStorage.setItem("UserLevel",result.data.userLevel);
                    sessionStorage.setItem("ProfileImage",result.data.profileImage);
                }

                window.location = "Dashboard";
            } else {
                Swal.fire("Invalid Login!", "", "error");
            }
        }
    });
}

//on sign in button click
$("#btnSignin").on("click", function() {
    pleaseWait();
    UserValidate();
});

//detect keypress
$(document).keypress(function(e) {
    var keycode = e.keyCode ? e.keyCode : e.which;

    //if Enter key press
    if (keycode == "13") {
        $("#btnSignin").trigger("click");
    }
});

//please wait
function pleaseWait() {
    Swal.fire({
        imageUrl: "/img/loading5.gif",
        showConfirmButton: false,
        allowOutsideClick: false
    });
}