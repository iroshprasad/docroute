//on page load
$(document).ready(function () {
  transferedDocuments();
  loadAllUsers();
});

//load transfered documents
function transferedDocuments() {
  var userid = sessionStorage.getItem("Userid");

  $.ajax({
    type: "POST",
    url: "Dashboard/GetTransferedDocs",
    data: {
      userId: userid,
    },
    error: function (result) {
      console.log(result);
    },
    success: function (result) {
      console.log(result);
      if (
        result.status == true &&
        result.message == "success" &&
        result.data.length > 0
      ) {
        $("#tbodyTransDoc").html("");
        var tbody = "";
        for (var i in result.data) {
          tbody +=
            "<tr data-userid='" +
            result.data[i].doc.transferFromUserId +
            "' data-documentid='" +
            result.data[i].doc.documentId +
            "' data-remark='" +
            result.data[i].doc.remark +
            "'>";
          tbody += '<th scope="row">';
          tbody += '<div class="media align-items-center">';
          tbody +=
            '<a href="#" class="avatar rounded-circle mr-3" style="width: 30px; height: 30px;">';

          var profileImage =
            result.data[i].uf.profileImage == "noimage.png"
              ? "/img/noimage.png"
              : "/img/userimgs/" +
                result.data[i].doc.transferFromUserId +
                "/" +
                result.data[i].uf.profileImage;

          tbody += '<img alt="Image placeholder" src="' + profileImage + '">';
          tbody += "</a>";
          tbody += '<div class="media-body">';
          tbody +=
            '<span class="mb-0 text-sm">' +
            result.data[i].uf.firstName +
            "</span>";
          tbody += "</div>";
          tbody += "</div>";
          tbody += "</th>";

          var tranDate = new Date(
            result.data[i].doc.transferDate.split("T")[0] +
              "T" +
              result.data[i].doc.transferTime
          );

          var documentPath = getDocumentUploadPath(
            result.data[i].docup.documentId +
              "/" +
              result.data[i].docup.documentPath
          );

          tbody += "<td>" + formatDate(tranDate) + "</td>";
          tbody += "<td>";
          tbody +=
            '<a href="' +
            documentPath +
            '" target="_blank" class="avatar rounded-circle mr-3" style="width: 30px; height: 30px;" data-toggle="tooltip" data-original-title="View">';
          tbody +=
            '<img alt="Image placeholder" src="/img/filelogo.png"  class="rounded-circle">';
          tbody += "</a>";
          tbody += "</td>";
          tbody += '<td class="text-right">';
          tbody +=
            '		<a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
          tbody += '	<i class="fas fa-ellipsis-v"></i>';
          tbody += "	</a>";
          tbody +=
            '		<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">';
          tbody +=
            '			<a class="dropdown-item"  href="#" onclick="transferModalPop(' +
            result.data[i].docup.documentId +
            ')" data-backdrop="static" data-keyboard="false"><i class="ni ni-ruler-pencil"></i> Transfer</a>';
          tbody +=
            '			<a class="dropdown-item viewRemark" href="#" data-backdrop="static" data-keyboard="false"><i class="ni ni-align-left-2"></i> View</a>';
          tbody += "	</div>";
          tbody += "	</td>";
          tbody += "	</tr>"; 
        }
        $("#tbodyTransDoc").html(tbody);
      }
    },
  });
}

//on click transfer document
function transferModalPop(documentId) {
  if (documentId != null && documentId != "") {
    $("#ModalTransferDocument").modal("show");
    $("#inputHiddenDocumentId").val(documentId);
  }
}

//on transfer click
$("#btnTransferDocument").on("click", function () {
  var hidDocId = $("#inputHiddenDocumentId").val();
  var remark = $("#inputTransferRemark").val();
  var fromUserId = sessionStorage.getItem("Userid");
  var toUserId = $("#inputTransferUser").val();

  if (toUserId == null || toUserId == "" || toUserId == "0") {
    Swal.fire("Please select a user to transfer", "", "warning");
    return false;
  }

  if (fromUserId == toUserId) {
    Swal.fire("Cannot transfer document to yourself", "", "warning");
    return false;
  }

  if (parseInt(hidDocId) > 0) {
    $.ajax({
      type: "POST",
      url: "UploadHistory/CheckAlreadyTransfer",
      data: {
        documentId: hidDocId,
        userId: toUserId,
      },
      error: function (result) {
        Swal.fire("Transfer unsuccess!", "", "error");
      },
      success: function (result) {
        console.log(result);
        if (result.status == true && result.message == "success") {
          Swal.fire(
            "Document already tranfered to the selected user!",
            "",
            "warning"
          );
        } else {
          transferDocument(hidDocId, remark, fromUserId, toUserId);
        }
      },
    });
  }
});

//function document transfer
function transferDocument(hidDocId, remark, fromUserId, toUserId) {
  $.ajax({
    type: "POST",
    url: "UploadHistory/TransferDocument",
    data: {
      DocumentId: hidDocId,
      Remark: remark,
      TransferFromUserId: fromUserId,
      TransferToUserId: toUserId,
      TransferTime: getNowTime(),
      TransferDate: getNowDate(),
    },
    error: function (result) {
      console.log(result);
      Swal.fire("Transfer unsuccess!", "", "error");
    },
    success: function (result) {
      console.log(result);
      if (result.status == true && result.message == "success") {
        Swal.fire("Transfer success!", "", "success");
        $("#btnCancelTransfer").trigger("click");
      } else {
        Swal.fire("Transfer unsuccess!", "", "error");
      }
    },
  });
}

//on transfer canceled
$("#btnCancelTransfer").on("click", function () {
  $("#inputTransferUser").val("");
  $("#inputTransferRemark").val("");
  $("#inputHiddenDocumentId").val("");
});

//load all users
function loadAllUsers() {
  $.ajax({
    type: "POST",
    url: "User/GetAllUsers",
    data: {
      Status: 1,
      Userlevel: 2,
    },
    error: function (result) {
      console.log(result);
    },
    success: function (result) {
      console.log(result);

      if (
        result.status == true &&
        result.message == "success" &&
        result.data.length > 0
      ) {
        var uselect = "";
        $("#inputTransferUser").html("");
        uselect += '<option selected value="">- Select -</option>';
        for (user in result.data) {
          uselect +=
            '<option value="' +
            result.data[user].userid +
            '">' +
            result.data[user].firstName +
            "</option>";
        }
        $("#inputTransferUser").html(uselect);
      }
    },
  });
}
 
//view Remark
$("#tbodyTransDoc").on("click", ".viewRemark", function () {
  var remark = $(this).closest("tr").data("remark");

  if(remark != null || remark != "") {
    Swal.fire(remark);
  } 
});