//on page load
$(document).ready(function () {
  loadUploadHistory();
  loadAllUsers();
  $("#btnCancelTransfer").trigger("click");
});

//on add document click
$("#btnModalDocument").on("click", function () {
  $("#ModalDocument").modal("show");
});

//on document upload click
$("#btnUploadDocument").on("click", function () {
  pleaseWait();

  var userId = sessionStorage.getItem("Userid");
  var selectedFile = document.getElementById("inputDocument").files;
  var remark = $("#inputRemark").val();
  var status = $("#inputStatus").prop("checked") == true ? 1 : 0;

  // FileReader function for read the file.
  var fileReader = new FileReader();
  var base64;
  //Check File is not Empty
  if (selectedFile.length > 0) {
    // Select the very first file from list
    var fileToLoad = selectedFile[0];

    //get file extension
    var fileExtension = selectedFile[0].name.split(".")[1];

    // Onload of file read the file content
    fileReader.onload = function (fileLoadedEvent) {
      base64 = fileLoadedEvent.target.result;

      $.ajax({
        type: "POST",
        url: "UploadHistory/SaveDocument",
        data: {
          DocumentPath: base64,
          UserId: userId,
          Remark: remark,
          Status: status,
          FileExtension: fileExtension,
          UploadDate: getNowDate(),
          UploadTime: getNowTime(),
        },
        error: function (result) {
          Swal.fire("Save unsuccess!", "", "error");
        },
        success: function (result) {
          console.log(result);
          if (result.status == true && result.message == "success") {
            Swal.fire("Save success!", "", "success").then(function () {
              clearAll();
              loadUploadHistory();
              showQRCode(result.documentno); 
            });
          } else {
            Swal.fire("Save unsuccess!", "", "error");
          }
        },
      });
    };
    // Convert data to base64
    fileReader.readAsDataURL(fileToLoad);
  } else {
    Swal.fire("Please select a file!", "", "warning");
  }
});

//on document seach click
$("#btnModalSearch").on("click", function () {
  $("#ModalDocumentSearch").modal("show");
});

//on search cancel button click
$("#btnCancelDocumentSearch").on("click", function () {
  $("#tbFoundFile").html("");
  $("#inputDocSearchText").val("");
  $("#inputDocumentNo").val("");
});

//on document seach click
$("#btnSearchDocument").on("click", function () {
  var searchText = $("#inputDocSearchText").val();
  var documentNo = $("#inputDocumentNo").val();
  var userId = sessionStorage.getItem("Userid");

  pleaseWait("/img/docread.gif");

  if (
    searchText.trim().length <= 0 ||
    searchText.trim() != "" ||
    documentNo.trim().length <= 0 ||
    documentNo.trim() != ""
  ) {
    $.ajax({
      type: "POST",
      url: "UploadHistory/ReadDocument",
      data: {
        searchText: searchText,
        userId: userId,
        documentno: documentNo,
      },
      error: function (result) {
        Swal.fire("Search unsuccess!", "", "error");
        console.log(result);
      },
      success: function (result) {
        $("#tbFoundFile").html("");

        if (result.status == true && result.message == "success") {
          var rows = "";
          var files = result.file.split(",");
          files.forEach((file) => {
            var filename = file.split("\\");
            rows += "<tr>";
            rows += "<td>" + filename[filename.length - 1] + "</td>";
            rows += "<td class='text-right'>";
            rows +=
              "<a target='_blank' href='documents" +
              file.split("documents").pop() +
              "' class='btn btn-success btn-sm'>View</a>";
            rows += "</td>";
            rows += "</tr>";
          });

          $("#tbFoundFile").html(rows);

          Swal.fire("Search found!", "", "success");
        } else {
          if (result.status == true && result.message == "nofound") {
            Swal.fire("No found!", "", "error");
          } else {
            Swal.fire("Search unsuccess!", "", "error");
          }
        }
        console.log(result);
      },
    });
  } else {
    Swal.fire("Search text or Document no required!", "", "warning");
    return false;
  }
});

//clear inputs
function clearAll() {
  document.getElementById("inputDocument").value = null;
  $("#inputRemark").val("");
  $("#inputStatus").prop("checked", true);
}

//load uploaded documents
function loadUploadHistory() {
  $.ajax({
    type: "POST",
    url: "UploadHistory/GetAllUploadDocuments",
    error: function (result) {
      console.log(result);
    },
    success: function (result) {
      if (result.status == true && result.message == "success") {
        var tbodyUploadDocuments = "";

        $("#tbodyUploadDocuments").html("");

        for (i in result.data) {
          var documentPath =
            result.data[i].d.documentId + "/" + result.data[i].d.documentPath;

          var uploadedOn =
            result.data[i].d.uploadDate.split("T")[0] +
            " " +
            result.data[i].d.uploadTime;

          tbodyUploadDocuments +=
            '<tr data-remark="' +
            result.data[i].d.remark +
            '" data-documentid="' +
            result.data[i].d.documentId +
            '" data-userid="' +
            result.data[i].u.userid +
            '">';
          tbodyUploadDocuments +=
            "<td><a style='color:white' onclick='showQRCode(" +
            result.data[i].d.documentno +
            ")'>" +
            result.data[i].d.documentno;
          +"</a></td>";
          tbodyUploadDocuments +=
            "<td>" + formatDate(new Date(uploadedOn)) + "</td>";

          if (result.data[i].d.status == 1) {
            tbodyUploadDocuments +=
              '<td><span class="badge badge-dot mr-4"><i class="bg-success"></i> active</span></td>';
          } else {
            tbodyUploadDocuments +=
              '<td><span class="badge badge-dot mr-4"><i class="bg-danger"></i> inactive</span></td>';
          }

          tbodyUploadDocuments += "<td>";
          tbodyUploadDocuments +=
            '<a href="' +
            getDocumentUploadPath(documentPath) +
            '" target="_blank" class="avatar rounded-circle mr-3" data-toggle="tooltip" data-original-title="View" style="width: 30px; height: 30px;">';
          tbodyUploadDocuments +=
            '<img alt="Image placeholder" src="/img/filelogo.png" class="rounded-circle"></a>';
          tbodyUploadDocuments += "</td>";

          tbodyUploadDocuments += '<td class="text-right">';
          tbodyUploadDocuments +=
            '		<a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
          tbodyUploadDocuments += '	<i class="fas fa-ellipsis-v"></i>';
          tbodyUploadDocuments += "	</a>";
          tbodyUploadDocuments +=
            '		<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">';
          tbodyUploadDocuments +=
            '			<a class="dropdown-item" onclick="transferModalPop(' +
            result.data[i].d.documentId +
            ')" href="#" data-backdrop="static" data-keyboard="false"><i class="ni ni-ruler-pencil"></i> Transfer</a>';
          tbodyUploadDocuments +=
            '			<a class="dropdown-item viewRemark" href="#" data-backdrop="static" data-keyboard="false"><i class="ni ni-align-left-2"></i> View</a>';
          tbodyUploadDocuments += "	</div>";
          tbodyUploadDocuments += "	</td>";

          tbodyUploadDocuments += "</tr>";
        }

        $("#tbodyUploadDocuments").html(tbodyUploadDocuments);
      }
    },
  });
}

//on click transfer document
function transferModalPop(documentId) {
  if (documentId != null && documentId != "") {
    $("#ModalTransferDocument").modal("show");
    $("#inputHiddenDocumentId").val(documentId);
  }
}

//load all users
function loadAllUsers() {
  $.ajax({
    type: "POST",
    url: "User/GetAllUsers",
    data: {
      Status: 1,
      Userlevel: 2,
    },
    error: function (result) {
      console.log(result);
    },
    success: function (result) {
      console.log(result);

      if (
        result.status == true &&
        result.message == "success" &&
        result.data.length > 0
      ) {
        var uselect = "";
        $("#inputTransferUser").html("");
        uselect += '<option selected value="">- Select -</option>';
        for (user in result.data) {
          uselect +=
            '<option value="' +
            result.data[user].userid +
            '">' +
            result.data[user].firstName +
            "</option>";
        }
        $("#inputTransferUser").html(uselect);
      }
    },
  });
}

//on transfer click
$("#btnTransferDocument").on("click", function () {
  var hidDocId = $("#inputHiddenDocumentId").val();
  var remark = $("#inputTransferRemark").val();
  var fromUserId = sessionStorage.getItem("Userid");
  var toUserId = $("#inputTransferUser").val();

  if (toUserId == null || toUserId == "" || toUserId == "0") {
    Swal.fire("Please select a user to transfer", "", "warning");
    return false;
  }

  if (fromUserId == toUserId) {
    Swal.fire("Cannot transfer document to yourself", "", "warning");
    return false;
  }

  if (parseInt(hidDocId) > 0) {
    $.ajax({
      type: "POST",
      url: "UploadHistory/CheckAlreadyTransfer",
      data: {
        documentId: hidDocId,
        userId: toUserId,
      },
      error: function (result) {
        Swal.fire("Transfer unsuccess!", "", "error");
      },
      success: function (result) {
        console.log(result);
        if (result.status == true && result.message == "success") {
          Swal.fire(
            "Document already tranfered to the selected user!",
            "",
            "warning"
          );
        } else {
          transferDocument(hidDocId, remark, fromUserId, toUserId);
        }
      },
    });
  }
});

//function document transfer
function transferDocument(hidDocId, remark, fromUserId, toUserId) {
  $.ajax({
    type: "POST",
    url: "UploadHistory/TransferDocument",
    data: {
      DocumentId: hidDocId,
      Remark: remark,
      TransferFromUserId: fromUserId,
      TransferToUserId: toUserId,
      TransferTime: getNowTime(),
      TransferDate: getNowDate(),
    },
    error: function (result) {
      console.log(result);
      Swal.fire("Transfer unsuccess!", "", "error");
    },
    success: function (result) {
      console.log(result);
      if (result.status == true && result.message == "success") {
        Swal.fire("Transfer success!", "", "success");
        $("#btnCancelTransfer").trigger("click");
      } else {
        Swal.fire("Transfer unsuccess!", "", "error");
      }
    },
  });
}

//on transfer canceled
$("#btnCancelTransfer").on("click", function () {
  $("#inputTransferUser").val("");
  $("#inputTransferRemark").val("");
  $("#inputHiddenDocumentId").val("");
});

//generate qr code
function showQRCode(documentno) {
  $("#ModalDocumentQRcode").modal("show");
  $("#documentNo").text("Document No " + documentno);
  $("#qrcode").html("");

  jQuery("#qrcode").qrcode({
    width: 250,
    height: 250,
    text: "" + documentno,
  });
}

//print qr code
function QRCode() {
  printJS("qrcode", "html");
}

//view Remark
$("#tbodyUploadDocuments").on("click", ".viewRemark", function () {
  var remark = $(this).closest("tr").data("remark");

  if (remark != null || remark != "") {
    Swal.fire(remark);
  }
});
