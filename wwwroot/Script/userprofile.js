//page on load
$(document).ready(function () {
  loadUserlevel();
  loadLoginUserDetails();
});

//confirm password
$("#inputPassword, #inputconfirmPass").on("keyup", function () {
  if ($("#inputPassword").val() == $("#inputconfirmPass").val()) {
    $("#labelPasswordMatch")
      .html("Matching")
      .attr("class", "text-success font-weight-700");
  } else {$("#labelPasswordMatch").html("Not Matching").attr("class", "text-danger font-weight-700");}
});

//function load userlevel
function loadUserlevel() {
  $.ajax({
    type: "POST",
    url: "Userlevel/GetAllUserlevels",
    error: function (result) {
      console.log(result);
    },
    success: function (result) {
      console.log(result);

      if (result.status == true && result.message == "success") {
        $("#inputUserLevel").html("");

        var selectUserlevel = '<option value="0">-Select-</option>';
        for (d in result.data) {
          if (result.data[d].status == 1) {
            selectUserlevel +=
              '<option value="' +
              result.data[d].userLevelId +
              '">' +
              result.data[d].userLevelName +
              "</option>";
          }
        }
        $("#inputUserLevel").html(selectUserlevel);
      }
    },
  });
}

//on cancel click
$("#btnCancelUser").on("click", function () {
  loadLoginUserDetails();
});

//on save button click
$("#btnSaveUser").on("click", function () {
  var firstName = $("#inputUserFirstName").val();
  var lastName = $("#inputUserLastName").val();
  var userEmail = $("#inputEmail").val();
  var userDOB = $("#inputDOB").val();

  if (
    firstName.trim().length <= 0 ||
    lastName.trim().length <= 0 ||
    userEmail.trim().length <= 0 ||
    userDOB == null ||
    userDOB == ""
  ) {
    Swal.fire("Fill the required!", "", "warning");
    return false;
  } else {
    checkEmail();
  }
});

//check email already exists
function checkEmail() {
  var hidUserId = $("#inputHiddenUserId").val();
  var inputEmail = $("#inputEmail").val();

  $.ajax({
    type: "POST",
    url: "User/GetUserByEmail",
    data: {
      Email: inputEmail,
    },
    error: function (result) {
      Swal.fire("Save unsuccess!", "", "error");
    },
    success: function (result) {
      pleaseWait();
      console.log(result);
      if (
        result.status == true &&
        result.message == "exist" &&
        hidUserId != result.data.userid
      ) {
        Swal.fire("User already exists!", "", "error");
      } else {
        updateUser();
      }
    },
  });
}

//function user update
function updateUser() {
  var firstName = $("#inputUserFirstName").val();
  var lastName = $("#inputUserLastName").val();
  var userEmail = $("#inputEmail").val();
  var userPhone = $("#inputPhone").val();
  var userDOB = $("#inputDOB").val();
  var hidUserId = $("#inputHiddenUserId").val();
  var imgProfileImage = $("#imgProfileImage").prop("src");

  //if not valid base64 string empty image src
  var _base64Image = imgProfileImage.split(",");
  imgProfileImage = _base64Image[1];
  if (isBase64(imgProfileImage) == false) {
    imgProfileImage = "noimage";
  }

  $.ajax({
    type: "POST",
    url: "User/UpdateUser",
    data: {
      UserId: hidUserId,
      Email: userEmail,
      FirstName: firstName,
      SecondName: lastName,
      ProfileImage: "",
      Phoneno: userPhone == "" ? " " : userPhone,
      DateOfBirth: userDOB,
    },
    error: function (result) {
      Swal.fire("Save unsuccess!", "", "error");
    },
    success: function (result) {
      if (result.status == true && result.message == "success") {
        Swal.fire("Save success!", "", "success").then(function () {
          $("#btnCancelUser").trigger("click");
        });

        // //save profile image
        $.ajax({
          type: "POST",
          url: "User/SaveImage",
          data: {
            base64image: imgProfileImage,
            userId: hidUserId,
            isNewUser: false,
          },
          error: function (result) {
            console.log(result);
          },
          success: function (result) {
            console.log(result);
          },
        });
      } else {
        Swal.fire("Save unsuccess!", "", "error");
      }
    },
  });
}

//get login user details
function loadLoginUserDetails() {
  var userid = sessionStorage.getItem("Userid");

  $.ajax({
    type: "POST",
    url: "UserProfile/GetUserDetails",
    data: {
      userId: userid,
    },
    error: function (result) {
      console.log(result);
    },
    success: function (result) {
      console.log(result);

      if (result.status == true && result.message == "success") {
        $("#inputHiddenUserId").val(result.data.userid);
        $("#inputUserFirstName").val(result.data.firstName);
        $("#inputUserLastName").val(result.data.secondName);
        $("#inputEmail").val(result.data.email);
        $("#inputDOB").val(result.data.dateOfBirth.split("T")[0]);
        $("#inputPhone").val(result.data.phoneno);
        $("#imgProfileImage").prop(
          "src",
          "img/userimgs/" + result.data.userid + "/" + result.data.profileImage
        );

        sessionStorage.clear();
        sessionStorage.setItem("Userid", result.data.userid);
        sessionStorage.setItem("Email", result.data.email);
        sessionStorage.setItem("FirstName", result.data.firstName);
        sessionStorage.setItem("SecondName", result.data.secondName);
        sessionStorage.setItem("UserLevel", result.data.userLevel);
        sessionStorage.setItem("ProfileImage", result.data.profileImage);

        $(".profileName").html(sessionStorage.getItem("FirstName"));

        if (sessionStorage.getItem("ProfileImage") == "noimage.png") {
          var profileImage = "/img/noimage.png";
          $(".profileImage").prop("src", profileImage);
        } else {
          var profileImage =
            "/img/userimgs/" +
            sessionStorage.getItem("Userid") +
            "/" +
            sessionStorage.getItem("ProfileImage");
          $(".profileImage").prop("src", profileImage);
        }
      }
    },
  });
}

//on password change button click
$("#btnChangePassword").on("click", function () {
  var currentPassword = $("#inputCurrentPassword").val();
  var newPassword = $("#inputPassword").val();
  var hidUserId = $("#inputHiddenUserId").val();
  var confirmPassword = $("#inputconfirmPass").val();

  if (hidUserId == null || hidUserId == "") {
    return false;
  }

  if (
    currentPassword == null ||
    currentPassword == "" ||
    newPassword == null ||
    newPassword == ""
  ) {
    Swal.fire("Current and New password required!", "", "warning");
    return false;
  }

  if (confirmPassword != newPassword) {
    Swal.fire("Password mismatch!", "", "warning");
    return false;
  }

  $.ajax({
    type: "POST",
    url: "UserProfile/ChangePassword",
    data: {
      currentPassword: currentPassword,
      newPassword: newPassword,
      userId: hidUserId,
    },
    error: function (result) {
      Swal.fire("Save unsuccess!", "", "error");
    },
    success: function (result) {
      pleaseWait();
      if (result.status == true && result.message == "nopassword") {
        Swal.fire("Current password wrong", "", "warning");
      } else if (result.status == true && result.message == "success") {
        Swal.fire("Password changed success", "", "success").then(function() {
          $("#btnCancelPassword").trigger("click");
        });
      }
    },
  });
});

//password change cancel on click
$("#btnCancelPassword").on("click", function () {
  $("#inputCurrentPassword").val("");
  $("#inputPassword").val("");
  $("#inputconfirmPass").val("");
  $("#labelPasswordMatch")
    .html("Not Matching")
    .attr("class", "text-danger font-weight-700");
});
