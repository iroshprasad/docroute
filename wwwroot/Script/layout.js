//on logout button click
$(".btnLogout").on("click", function() {
  pleaseWait();
  window.location = "Login";
});

//on page load
$(document).ready(function() {
  loadUserDetails();
})


//funcion load logged in user details
function loadUserDetails(){
  $(".profileName").html(sessionStorage.getItem("FirstName"));  

  if(sessionStorage.getItem("ProfileImage") == "noimage.png"){
    var profileImage = "/img/noimage.png";
    $(".profileImage").prop("src", profileImage);
  }else{
    var profileImage = "/img/userimgs/" + sessionStorage.getItem("Userid") + "/"+ sessionStorage.getItem("ProfileImage");
    $(".profileImage").prop("src", profileImage);
  } 
}

//my profile on click
$(".btnmyProfile").on("click", function(){
  pleaseWait(); 
  window.location = "UserProfile";
});

 
