//function formate date (d-m-y 9:18 pm)
function formatDate(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? "pm" : "am";
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? "0" + minutes : minutes;
  var strTime = hours + ":" + minutes + " " + ampm;

  var date1 = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
  var month1 =
    date.getMonth() + 1 < 10
      ? "0" + (date.getMonth() + 1)
      : date.getMonth() + 1;
  var year1 = date.getFullYear();
  //return date.getDate() + "/" + date.getMonth() + 1 +"/" + date.getFullYear() + " " + strTime;
  return year1 + "-" + month1 + "-" + date1 + " " + strTime;
}

function getNowDate() {
  var date = new Date();
  var year = date.getFullYear();
  let month = (date.getMonth() + 1 < 10 ? "0" : "") + (date.getMonth() + 1); //month start with 0
  var day = (date.getDate() < 10 ? "0" : "") + date.getDate();

  var today = year + "-" + month + "-" + day;
  return today;
}

function getNowTime() {
  var date = new Date();
  var hour = date.getHours();
  var minute = date.getMinutes(); //month start with 0
  var second = date.getSeconds();

  var now = hour + ":" + minute + ":" + second;
  return now;
}

//please wait
function pleaseWait(imgUrl = "/img/loading5.gif") {
  Swal.fire({
    imageUrl: imgUrl,
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}

//function get image src and set
function setImage(input, output) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $("#" + output).attr("src", e.target.result);
    };

    reader.readAsDataURL(input.files[0]);
  }
}

//function check is numeric
function isNumberKey(e) {
  if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
    return false;
  }
}

//functoin get basepath from full path
function getBasePath(fullPath) {
  var pathPieces = fullPath.split("/");
  var basepath = pathPieces[pathPieces.length - 1];
  return basepath;
}

//function check is base64
// function isBase64(str) {
//   try {
//     return btoa(atob(str)) == str;
//   } catch (err) {
//     return false;
//   }
// }
function isBase64(str) {
  var base64Matcher = new RegExp(
    "^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$"
  );

  if (base64Matcher.test(str)) {
    return true;
  } else {
    return false;
  }
}

function getDocumentUploadPath(documentPath) {
  var folderPath = "\\documents\\" + documentPath;
  return folderPath;
}
