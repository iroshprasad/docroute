//on page load
$(document).ready(function() {
  $("#btnCancelDepartment").trigger("click");
  loadDepartment();
});

//on add department click
$("#btnModalDepartment").on("click", function() {
  $("#modalDepartment").modal("show");
  $("#btnCancelDepartment").trigger("click");
});
 

//on modal popup department cancel click
$("#btnCancelDepartment").on("click", function() {
  $("#inputDepartmentName").val("");
  $("#inputStatus").prop("checked", true);
  $("#inputHiddenDepartmentId").val("");
});

//on modal save department save click
$("#btnSaveDepartment").on("click", function() {
  //check already exists
  getDepartmentByName();
});

//function save department
function saveDepartment(inputDepartmentName, inputStatus) {
  $.ajax({
    type: "POST",
    url: "Department/SaveDepartment",
    data: {
      DepartmentName: inputDepartmentName,
      Status: inputStatus,
    },
    error: function (result) {
      console.log(result);
      Swal.fire("Save unsuccess!", "", "error");
    },
    success: function (result) {
      console.log(result);
      if (result.status == true && result.message == "success") {
        Swal.fire("Save success!", "", "success");
        loadDepartment();
        $("#btnCancelDepartment").trigger("click");
      } else {
        Swal.fire("Save unsuccess!", "", "error");
      }
    },
  });
}

//function update department
function updateDepartment(hidDepartmentId, inputDepartmentName, inputStatus) {
  $.ajax({
    type: "POST",
    url: "Department/UpdateDepartment",
    data: {
      DepartmentId: hidDepartmentId,
      DepartmentName: inputDepartmentName,
      Status: inputStatus
    },
    error: function(result) {
      Swal.fire("Save unsuccess!", "", "error");
    },
    success: function(result) {
      if (result.status == true && result.message == "success") {
        Swal.fire("Save success!", "", "success");
        loadDepartment();
        $("#btnCancelDepartment").trigger("click");
      } else {
        Swal.fire("Save unsuccess!", "", "error");
      }
    }
  });
}

//function get department by name
function getDepartmentByName() {
  var inputDepartmentName = $("#inputDepartmentName").val();
  var inputStatus = $("#inputStatus").prop("checked") == true ? 1 : 0;
  var hidDepartmentId =
    $("#inputHiddenDepartmentId").val() == ""
      ? "0"
      : $("#inputHiddenDepartmentId").val();

  if (inputDepartmentName.length <= 0 || inputDepartmentName.trim() == "") {
    Swal.fire("Fill the required!", "", "warning");
    return;
  }

  $.ajax({
    type: "POST",
    url: "Department/GetDepartmentByName",
    data: {
      DepartmentName: inputDepartmentName 
    },
    error: function(result) {
      Swal.fire("Save unsuccess!", "", "error");
    },
    success: function(result) {
      console.log(result);
      pleaseWait();

      if (result.status == true) {
        //if modify exist department
        if (
          (result.message == "exist" &&
            hidDepartmentId == result.data.departmentId) ||
          (result.message == "notexist" && hidDepartmentId != "0")
        ) {
          updateDepartment(hidDepartmentId, inputDepartmentName, inputStatus);
        } else if (result.message == "notexist" && hidDepartmentId == "0") {
          saveDepartment(inputDepartmentName, inputStatus);
        } else {
          Swal.fire("Department already exists!", "", "error");
        }
      }

      if (result.status == false && result.message == "failed") {
        Swal.fire("Save unsuccess!", "", "error");
      }
    }
  });
}

//function load departments
function loadDepartment() {
  $.ajax({
    type: "POST",
    url: "Department/GetAllDepartments",
    error: function(result) {
      console.log(result);
    },
    success: function(result) {
      console.log(result);

      if (result.status == true && result.message == "success") {
        var tableBodyDepartment = "";

        $("#tableBodyDepartment").html("");

        for (i in result.data) {
          tableBodyDepartment +=
            '<tr id="' + result.data[i].departmentId + '">';
          tableBodyDepartment +=
            "<td>" + result.data[i].departmentName + "</td>";

          if (result.data[i].status == 1) {
            tableBodyDepartment +=
              '<td><span class="badge badge-dot mr-4"><i class="bg-success"></i> active</span></td>';
          } else {
            tableBodyDepartment +=
              '<td><span class="badge badge-dot mr-4"><i class="bg-danger"></i> inactive</span></td>';
          }

          tableBodyDepartment += "<td>";
          tableBodyDepartment +=
            '<a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
          tableBodyDepartment += '<i class="fas fa-ellipsis-v"></i>';
          tableBodyDepartment += "</a>";
          tableBodyDepartment +=
            '<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">';
          tableBodyDepartment +=
            '<a class="dropdown-item viewDepartment" href="#"><i class="ni ni-ruler-pencil"></i> View</a>';
          tableBodyDepartment +=
            '<a class="dropdown-item deleteDepartment" href="#"><i class="ni ni-fat-remove"></i> Delete</a>';
          tableBodyDepartment += "</div>";
          tableBodyDepartment += "</td>";
          tableBodyDepartment += "</tr>";
        }

        $("#tableBodyDepartment").html(tableBodyDepartment);
      }
    }
  });
}

//on department table row delete click
$("#tableBodyDepartment").on("click", ".deleteDepartment", function() {
  var departmentId = $(this)
    .closest("tr")
    .attr("id");

  Swal.fire({
    title: "Are you sure?",
    text: "You won't be able to revert this!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Yes"
  }).then(result => {
    pleaseWait();

    if (result.value) {
      $.ajax({
        type: "POST",
        url: "Department/DeleteDepartment",
        data: {
          DepartmentId: departmentId
        },
        error: function(result) {
          Swal.fire("Delete unsuccess!", "", "error");
        },
        success: function(result) {
          console.log(result);

          if (result.status == true && result.message == "assigned") {
            Swal.fire("Delete unsuccess!", "Department already assigned to users", "warning");
            return;
          }

          if (result.status == true && result.message == "success") {
            Swal.fire("Delete success!", "", "success"); 
            loadDepartment();
          } else {
            Swal.fire("Delete unsuccess!", "", "error");
          }
        }
      });
    }
  });
});

//on department table row view click
$("#tableBodyDepartment").on("click", ".viewDepartment", function() {
  var departmentId = $(this)
    .closest("tr")
    .attr("id");

  $.ajax({
    type: "POST",
    url: "Department/GetDepartmentById",
    data: {
      DepartmentId: departmentId
    },
    error: function(result) {
      console.log(result);
    },
    success: function(result) {
      console.log(result);
      if (result.status == true && result.message == "success") {
        $("#btnCancelDepartment").trigger("click");

        $("#modalDepartment").modal("show");

        $("#inputDepartmentName").val(result.data.departmentName);
        $("#inputStatus").prop(
          "checked",
          result.data.status == 1 ? true : false
        );
        $("#inputHiddenDepartmentId").val(result.data.departmentId);
      }
    }
  });
});
