//on page load
$(document).ready(function () {
  $("#btnCancelUser").trigger("click");
  loadUser();
  loadDeapartment();
  loadUserlevel();
});

//on add User click
$("#btnModalUser").on("click", function () {
  $("#modalUser").modal("show");
  $("#btnCancelUser").trigger("click");
});

//on modal popup User cancel click
$("#btnCancelUser").on("click", function () {
  $("#inputHiddenUserId").val("");
  $("#inputEmail").val("");
  $("#inputFirstName").val("");
  $("#inputLastName").val("");
  $("#inputPassword").val("");
  $("#inputConfirmPassword").val("");
  $("#inputDOB").val("");
  $("#inputPhone").val("");
  $("#imgProfileImage").attr("src", "/img/noimage.png");
  $("#inputStatus").prop("checked", false);
  $("#inputUserlevel").val("0");
  $("#inputDepartment").val("0");
  $("#inputDepartmentHead").prop("checked", false); 
  $("#labelPasswordMatch").html("Not Matching").prop("class", "text-danger font-weight-700");
  $("#passwordLabelStrength").html("Very Weak").prop("class", "text-danger font-weight-700");
});

//on modal save User save click
$("#btnSaveUser").on("click", function () {
  //check already exists
  getUserByName();
});

//function save User
function saveUser(
  inputEmail,
  inputFirstName,
  inputLastName,
  inputPassword,
  inputDOB,
  inputPhone,
  inputStatus,
  inputUserlevel,
  inputDepartment,
  inputDepartmentHead,
  imgProfileImage
) {
  $.ajax({
    type: "POST",
    url: "User/SaveUser",
    data: {
      Email: inputEmail,
      UserLevel: inputUserlevel,
      FirstName: inputFirstName,
      SecondName: inputLastName,
      ProfileImage: "noimage.png",
      DepartmentId: inputDepartment,
      Password: inputPassword,
      Phoneno: inputPhone == "" ? " " : inputPhone,
      DateOfBirth: inputDOB,
      IsDepartmentHead: inputDepartmentHead,
      Status: inputStatus,
      CreatedTime: getNowTime(),
      CreatedDate: getNowDate(),
      VerifyCode: "0",
    },
    error: function (result) {
      console.log(result);
      Swal.fire("Save unsuccess!", "", "error");
    },
    success: function (result) {
      console.log(result);
      if (result.status == true && result.message == "success") {
        Swal.fire("Save success!", "", "success").then(function () {
          $("#btnCancelUser").trigger("click");
          loadUser();
        });

        //save profile image
        $.ajax({
          type: "POST",
          url: "User/SaveImage",
          data: {
            base64image: imgProfileImage,
            userId: result.lastInsertId,
          },
          error: function (result) {
            console.log(result);
          },
          success: function (result) {
            console.log(result);
          },
        });
      } else {
        Swal.fire("Save unsuccess!", "", "error");
      }
    },
  });
}

//function update User
function updateUser(
  hidUserId,
  inputEmail,
  inputFirstName,
  inputLastName,
  inputPassword,
  inputConfirmPassword,
  inputDOB,
  inputPhone,
  inputStatus,
  inputUserlevel,
  inputDepartment,
  inputDepartmentHead,
  imgProfileImage
) {
  if (
    inputPassword != null ||
    inputPassword.trim().length > 0 ||
    inputConfirmPassword != null ||
    inputConfirmPassword.trim().length > 0
  ) {
    if (inputPassword != inputConfirmPassword) {
      Swal.fire({
        icon: "warning",
        title: "Password mismatch",
        text: "",
      });
      return;
    }

    //update user with password
    updateUserWithPassword(
      hidUserId,
      inputEmail,
      inputFirstName,
      inputLastName,
      inputPassword,
      inputDOB,
      inputPhone,
      inputStatus,
      inputUserlevel,
      inputDepartment,
      inputDepartmentHead,
      imgProfileImage
    );
  } else {
    //update user without password
    updateUserWithoutPassword(
      hidUserId,
      inputEmail,
      inputFirstName,
      inputLastName,
      inputDOB,
      inputPhone,
      inputStatus,
      inputUserlevel,
      inputDepartment,
      inputDepartmentHead,
      imgProfileImage
    );
  }
}

//function update user with passsword
function updateUserWithPassword(
  hidUserId,
  inputEmail,
  inputFirstName,
  inputLastName,
  inputPassword,
  inputDOB,
  inputPhone,
  inputStatus,
  inputUserlevel,
  inputDepartment,
  inputDepartmentHead,
  imgProfileImage
) {
  $.ajax({
    type: "POST",
    url: "User/UpdateUser",
    data: {
      UserId: hidUserId,
      Email: inputEmail,
      UserLevel: inputUserlevel,
      FirstName: inputFirstName,
      SecondName: inputLastName,
      ProfileImage: "",
      DepartmentId: inputDepartment,
      Password: inputPassword,
      Phoneno: inputPhone == "" ? " " : inputPhone,
      DateOfBirth: inputDOB,
      IsDepartmentHead: inputDepartmentHead,
      Status: inputStatus,
      VerifyCode: "0",
    },
    error: function (result) {
      Swal.fire("Save unsuccess!", "", "error");
    },
    success: function (result) {
      if (result.status == true && result.message == "success") {
        Swal.fire("Save success!", "", "success").then(function () {
          $("#btnCancelUser").trigger("click");
          loadUser();
        });

        //save profile image
        $.ajax({
          type: "POST",
          url: "User/SaveImage",
          data: {
            base64image: imgProfileImage,
            userId: hidUserId,
            isNewUser: false,
          },
          error: function (result) {
            console.log(result);
          },
          success: function (result) {
            console.log(result);
          },
        });
      } else {
        Swal.fire("Save unsuccess!", "", "error");
      }
    },
  });
}

//function update user without passsword
function updateUserWithoutPassword(
  hidUserId,
  inputEmail,
  inputFirstName,
  inputLastName,
  inputDOB,
  inputPhone,
  inputStatus,
  inputUserlevel,
  inputDepartment,
  inputDepartmentHead,
  imgProfileImage
) {
  $.ajax({
    type: "POST",
    url: "User/UpdateUser",
    data: {
      UserId: hidUserId,
      Email: inputEmail,
      UserLevel: inputUserlevel,
      FirstName: inputFirstName,
      SecondName: inputLastName,
      ProfileImage: "",
      DepartmentId: inputDepartment,
      Phoneno: inputPhone == "" ? " " : inputPhone,
      DateOfBirth: inputDOB,
      IsDepartmentHead: inputDepartmentHead,
      Status: inputStatus,
      VerifyCode: "0",
    },
    error: function (result) {
      Swal.fire("Save unsuccess!", "", "error");
    },
    success: function (result) {
      if (result.status == true && result.message == "success") {
        Swal.fire("Save success!", "", "success").then(function () {
          $("#btnCancelUser").trigger("click");
          loadUser();
        });

        //save profile image
        $.ajax({
          type: "POST",
          url: "User/SaveImage",
          data: {
            base64image: imgProfileImage,
            userId: hidUserId,
            isNewUser: false,
          },
          error: function (result) {
            console.log(result);
          },
          success: function (result) {
            console.log(result);
          },
        });
      } else {
        Swal.fire("Save unsuccess!", "", "error");
      }
    },
  });
}

//function validate phone number is numeric
$("#inputPhone").on("keypress", function (e) {
  return isNumberKey(e);
});

//function get User by name
function getUserByName() {
  var hidUserId =
    $("#inputHiddenUserId").val() == "" ? "0" : $("#inputHiddenUserId").val();
  var inputEmail = $("#inputEmail").val();
  var inputFirstName = $("#inputFirstName").val();
  var inputLastName = $("#inputLastName").val();
  var inputPassword = $("#inputPassword").val();
  var inputConfirmPassword = $("#inputConfirmPassword").val();
  var inputDOB = $("#inputDOB").val();
  var inputPhone = $("#inputPhone").val() == "" ? "" : $("#inputPhone").val();
  var imgProfileImage = $("#imgProfileImage").prop("src");
  var inputStatus = $("#inputStatus").prop("checked") == true ? 1 : 0;
  var inputUserlevel = $("#inputUserlevel").val();
  var inputDepartment = $("#inputDepartment").val();
  var inputDepartmentHead =
    $("#inputDepartmentHead").prop("checked") == true ? 1 : 0;

  //if not valid base64 string empty image src
  var _base64Image = imgProfileImage.split(",");
  imgProfileImage = _base64Image[1];
  if (isBase64(imgProfileImage) == false) {
    imgProfileImage = "noimage";
  }

  if (
    inputEmail.length <= 0 ||
    inputEmail.trim() == "" ||
    inputFirstName.length <= 0 ||
    inputFirstName.trim() == "" ||
    inputLastName.length <= 0 ||
    inputLastName.trim() == "" ||
    inputUserlevel.length <= 0 ||
    inputUserlevel.trim() == "" ||
    inputUserlevel.trim() == "0" ||
    inputDepartment.length <= 0 ||
    inputDepartment.trim() == "" ||
    inputDepartment.trim() == "0"
  ) {
    Swal.fire("Fill the required!", "", "warning");
    return;
  }

  if (hidUserId.trim().length <= 0 || hidUserId == null || hidUserId <= "0") {
    if (
      inputPassword == null ||
      inputPassword.trim().length <= 0 ||
      inputConfirmPassword == null ||
      inputConfirmPassword.trim().length <= 0
    ) {
      Swal.fire({
        icon: "warning",
        title: "Password required",
        text: "",
      });
      return;
    } else {
      if (inputPassword != inputConfirmPassword) {
        Swal.fire({
          icon: "warning",
          title: "Password mismatch",
          text: "",
        });
        return;
      }
    }
  }

  $.ajax({
    type: "POST",
    url: "User/GetUserByEmail",
    data: {
      Email: inputEmail,
    },
    error: function (result) {
      Swal.fire("Save unsuccess!", "", "error");
    },
    success: function (result) {
      pleaseWait();
      console.log(result);
      if (result.status == true) {
        //if modify exist User
        if (
          (result.message == "exist" && hidUserId == result.data.userid) ||
          (result.message == "notexist" && hidUserId != "0")
        ) {
          updateUser(
            hidUserId,
            inputEmail,
            inputFirstName,
            inputLastName,
            inputPassword,
            inputConfirmPassword,
            inputDOB,
            inputPhone,
            inputStatus,
            inputUserlevel,
            inputDepartment,
            inputDepartmentHead,
            imgProfileImage
          );
        } else if (result.message == "notexist" && hidUserId == "0") {
          saveUser(
            inputEmail,
            inputFirstName,
            inputLastName,
            inputPassword,
            inputDOB,
            inputPhone,
            inputStatus,
            inputUserlevel,
            inputDepartment,
            inputDepartmentHead,
            imgProfileImage
          );
        } else {
          Swal.fire("User already exists!", "", "error");
        }
      }

      if (result.status == false && result.message == "failed") {
        Swal.fire("Save unsuccess!", "", "error");
      }
    },
  });
}

//function load Users
function loadUser() {
  var userid = sessionStorage.getItem("Userid");

  if (userid == null) {
    return false;
  }

  $.ajax({
    type: "POST",
    url: "User/GetAllUsers",
    data: {
      UserId: userid,
    },
    error: function (result) {
      console.log(result);
    },
    success: function (result) {
      console.log(result);

      if (result.status == true && result.message == "success") {
        var tableBodyUser = "";

        $("#tableBodyUser").html("");

        for (i in result.data) {
          var createdOn =
            result.data[i].createdDate.split("T")[0] +
            " " +
            result.data[i].createdTime;

          var userFullName =
            result.data[i].firstName + " " + result.data[i].secondName;
          var profileImage =
            result.data[i].profileImage == "noimage.png"
              ? "/img/noimage.png"
              : "/img/userimgs/" +
                result.data[i].userid +
                "/" +
                result.data[i].profileImage;
          var isDepartmentHead =
            result.data[i].isDepartmentHead == 1 ? "(Head)" : "";

          tableBodyUser += '<tr id="' + result.data[i].userid + '">';
          tableBodyUser += "<td>" + result.data[i].email + "</td>";
          tableBodyUser += "<td>" + result.data[i].userLevelName + "</td>";
          tableBodyUser += "<td>" + userFullName + "</td>";

          var birthDate =
            result.data[i].dateOfBirth == "0001-01-01T00:00:00"
              ? ""
              : result.data[i].dateOfBirth.split("T")[0];

          var phoneNo =
            result.data[i].phoneno == null ? "" : result.data[i].phoneno;

          tableBodyUser += "<td>" + birthDate + "</td>";
          tableBodyUser += "<td>" + phoneNo + "</td>";
          tableBodyUser +=
            '<td><a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="' +
            userFullName +
            '">';
          tableBodyUser +=
            '<img alt="Image placeholder" src="' +
            profileImage +
            '" class="rounded-circle"></a>';
          tableBodyUser += "</td>";
          tableBodyUser +=
            "<td>" +
            result.data[i].departmentName +
            " " +
            isDepartmentHead +
            "</td>";
          tableBodyUser += "<td>" + formatDate(new Date(createdOn)) + "</td>";

          if (result.data[i].status == 1) {
            tableBodyUser +=
              '<td><span class="badge badge-dot mr-4"><i class="bg-success"></i> active</span></td>';
          } else {
            tableBodyUser +=
              '<td><span class="badge badge-dot mr-4"><i class="bg-danger"></i> inactive</span></td>';
          }

          tableBodyUser += "<td>";
          tableBodyUser +=
            '<a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
          tableBodyUser += '<i class="fas fa-ellipsis-v"></i>';
          tableBodyUser += "</a>";
          tableBodyUser +=
            '<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">';
          tableBodyUser +=
            '<a class="dropdown-item viewUser" href="#"><i class="ni ni-ruler-pencil"></i> View</a>';
          tableBodyUser +=
            '<a class="dropdown-item deleteUser" href="#"><i class="ni ni-fat-remove"></i> Delete</a>';
          tableBodyUser += "</div>";
          tableBodyUser += "</td>";
          tableBodyUser += "</tr>";
        }

        $("#tableBodyUser").html(tableBodyUser);
      }
    },
  });
}

//on User table row delete click
$("#tableBodyUser").on("click", ".deleteUser", function () {
  var UserId = $(this).closest("tr").attr("id");

  Swal.fire({
    title: "Are you sure?",
    text: "You won't be able to revert this!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Yes",
  }).then((result) => {
    if (result.value) {
      pleaseWait();

      $.ajax({
        type: "POST",
        url: "User/DeleteUser",
        data: {
          UserId: UserId,
        },
        error: function (result) {
          Swal.fire("Delete unsuccess!", "", "error");
        },
        success: function (result) {
          console.log(result);
          if (result.status == true && result.message == "success") {
            Swal.fire("Delete success!", "", "success").then(function () {
              loadUser();
            });
          } else {
            Swal.fire("Delete unsuccess!", "", "error");
          }
        },
      });
    }
  });
});

//on User table row view click
$("#tableBodyUser").on("click", ".viewUser", function () {
  var userId = $(this).closest("tr").attr("id");

  $.ajax({
    type: "POST",
    url: "User/GetUserById",
    data: {
      Userid: userId,
    },
    error: function (result) {
      console.log(result);
    },
    success: function (result) {
      console.log(result);
      if (result.status == true && result.message == "success") {
        $("#btnCancelUser").trigger("click");

        $("#modalUser").modal("show");

        $("#inputEmail").val(result.data.email);
        $("#inputFirstName").val(result.data.firstName);
        $("#inputLastName").val(result.data.secondName);
        $("#inputDOB").val(
          result.data.dateOfBirth.split("T")[0] == "0001-01-01"? "" : result.data.dateOfBirth.split("T")[0]
        );
        $("#inputPhone").val(
          result.data.phoneno == " " ? "" : result.data.phoneno
        );
        $("#inputUserlevel").val(result.data.userLevel);
        $("#inputDepartment").val(result.data.departmentId);

        var profileImage =
          result.data.profileImage == "noimage.png"
            ? "/img/noimage.png"
            : "/img/userimgs/" +
              result.data.userid +
              "/" +
              result.data.profileImage;
        $("#imgProfileImage").attr("src", profileImage);

        $("#inputStatus").prop(
          "checked",
          result.data.status == 1 ? true : false
        );
        $("#inputDepartmentHead").prop(
          "checked",
          result.data.isDepartmentHead == 1 ? true : false
        );

        $("#inputHiddenUserId").val(result.data.userid);
      }
    },
  });
});

//function load department
function loadDeapartment() {
  $.ajax({
    type: "POST",
    url: "Department/GetAllDepartments",
    error: function (result) {
      console.log(result);
    },
    success: function (result) {
      console.log(result);

      if (result.status == true && result.message == "success") {
        $("#inputDepartment").html("");

        var selectDepartment = '<option value="0">-Select-</option>';
        for (d in result.data) {
          if (result.data[d].status == 1) {
            selectDepartment +=
              '<option value="' +
              result.data[d].departmentId +
              '">' +
              result.data[d].departmentName +
              "</option>";
          }
        }
        $("#inputDepartment").html(selectDepartment);
      }
    },
  });
}

//function load userlevel
function loadUserlevel() {
  $.ajax({
    type: "POST",
    url: "Userlevel/GetAllUserlevels",
    error: function (result) {
      console.log(result);
    },
    success: function (result) {
      console.log(result);

      if (result.status == true && result.message == "success") {
        $("#inputUserlevel").html("");

        var selectUserlevel = '<option value="0">-Select-</option>';
        for (d in result.data) {
          if (result.data[d].status == 1) {
            selectUserlevel +=
              '<option value="' +
              result.data[d].userLevelId +
              '">' +
              result.data[d].userLevelName +
              "</option>";
          }
        }
        $("#inputUserlevel").html(selectUserlevel);
      }
    },
  });
}

//function on confirm password key pressed
$("#inputConfirmPassword").on("keyup", function () {
  matchPassword();
});

//function on focus on confirm password input
$("#inputPassword").on("keyup", function () {
  matchPassword();
});

//function check password match
function matchPassword() {
  var password = $("#inputPassword").val();
  var confirmpassword = $("#inputConfirmPassword").val();

  if (password == confirmpassword) {
    $("#passwordLabelMatch").text("Matched");
    $("#passwordLabelMatch").prop("class", "text-success font-weight-700");
  } else {
    $("#passwordLabelMatch").text("Not Match");
    $("#passwordLabelMatch").prop("class", "text-danger font-weight-700");
  }
}

//function on input password key pressed
$("#inputPassword").on("keyup", function () {
  checkPasswordStrength();
});

//function check password strength
function checkPasswordStrength() {
  var password = $("#inputPassword").val();
  var no = 0;

  if (password != "") {
    // If the password length is less than or equal to 6
    if (password.length <= 6) {
      no = 1;
    }

    // If the password length is greater than 6 and contain any lowercase alphabet or any number or any special character
    if (
      password.length > 6 &&
      (password.match(/[a-z]/) ||
        password.match(/\d+/) ||
        password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))
    ) {
      no = 2;
    }

    // If the password length is greater than 6 and contain alphabet,number,special character respectively
    if (
      password.length > 6 &&
      ((password.match(/[a-z]/) && password.match(/\d+/)) ||
        (password.match(/\d+/) &&
          password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) ||
        (password.match(/[a-z]/) &&
          password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)))
    ) {
      no = 3;
    }

    // If the password length is greater than 6 and must contain alphabets,numbers and special characters
    if (
      password.length > 6 &&
      password.match(/[a-z]/) &&
      password.match(/\d+/) &&
      password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)
    ) {
      no = 4;
    }

    if (no == 1) {
      $("#passwordLabelStrength").text("Very Weak");
      $("#passwordLabelStrength").prop("class", "text-danger font-weight-700");
    }

    if (no == 2) {
      $("#passwordLabelStrength").text("Weak");
      $("#passwordLabelStrength").prop("class", "text-default font-weight-700");
    }

    if (no == 3) {
      $("#passwordLabelStrength").text("Good");
      $("#passwordLabelStrength").prop("class", "text-primary font-weight-700");
    }

    if (no == 4) {
      $("#passwordLabelStrength").text("Strong");
      $("#passwordLabelStrength").prop("class", "text-success font-weight-700");
    }
  }
} 