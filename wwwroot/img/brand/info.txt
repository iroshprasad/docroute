
Hope you enjoy your new logo, here are the people that
made your beautiful logo happen :)
font name: Fonarto
font link: https://www.behance.net/gallery/20085723/Fonarto-Typeface
font author: Arwan Sutanto
font author site: http://artonedigital.com/
Slogan Font: Questrial-Regular.otf

icon designer: useiconic.com
icon designer link: /useiconic.com
        
{"bg":"transparent","icon-gradient-0":"#B4373D","icon-gradient-1":"#B23ACB","font":"#000000","slogan":"#141414"}
      