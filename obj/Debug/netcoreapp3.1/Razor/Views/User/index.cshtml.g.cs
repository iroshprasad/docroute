#pragma checksum "C:\xampp\htdocs\DocRoute\Views\User\index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3efcb588353be60f891edbedc27c4d8280505a50"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_User_index), @"mvc.1.0.view", @"/Views/User/index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\xampp\htdocs\DocRoute\Views\_ViewImports.cshtml"
using DocRoute;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\xampp\htdocs\DocRoute\Views\_ViewImports.cshtml"
using DocRoute.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3efcb588353be60f891edbedc27c4d8280505a50", @"/Views/User/index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b190190ea675b21a69819cb0ad282a9bda0d265c", @"/Views/_ViewImports.cshtml")]
    public class Views_User_index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "0", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", new global::Microsoft.AspNetCore.Html.HtmlString("text/javascript"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\xampp\htdocs\DocRoute\Views\User\index.cshtml"
  
ViewData["Title"] = "User";
ViewData["PageDomain"] = "User";
ViewData["PageDescription"] = "You can create new user,edit and delete exising users.";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<!-- page top data add button -->
<div class=""row mt-3"">
	<div class=""col-xl-6 col-lg-6"">
		<button type=""button"" id=""btnModalUser"" data-backdrop=""static"" data-keyboard=""false"" class=""btn btn-info"">Add user</button>
	</div>
</div>

<div class=""row pb-8 pt-5 pt-md-4""> 
	<div class=""col"">
		<div class=""card bg-default shadow"">
			<div class=""card-header bg-transparent border-0"">
				<h3 class=""text-white mb-0"">
					All Users
				</h3>
			</div>
			<div class=""table-responsive"">
				<table class=""table align-items-center table-dark table-flush"">
					<thead class=""thead-dark"">
						<tr>
							<th scope=""col"">User Email</th>
							<th scope=""col"">Userlevel</th>
                            <th scope=""col"">Name</th>
                            <th scope=""col"">Birth On</th>
                            <th scope=""col"">Phone</th>
                            <th scope=""col"">Image</th>
                            <th scope=""col"">Department</th>
                            <th scope=""col"">Cr");
            WriteLiteral(@"eated On</th>
                            <th scope=""col"">Status</th>
							<th scope=""col""></th>
						</tr>
					</thead>
					<tbody id=""tableBodyUser"">  
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class=""modal fade"" id=""modalUser"" style=""top:25% !important;"" role=""modal"" tabindex=""-1"" data-backdrop=""false"" aria-labelledby=""modalUser"" aria-hidden=""true"">
<div class=""modal-dialog modal-lg"">
	<div class=""modal-content"">
		<div class=""card bg-secondary shadow"">
			<div class=""card-header bg-white border-0"">
				<div class=""row align-items-center"">
					<div class=""col-8"">
						<h3 class=""mb-0"">Create/Modify User</h3>
					</div>
					<div class=""col-4 text-right"">
						<button type=""button"" class=""close"" data-dismiss=""modal"">&times;</button>
					</div>
				</div>
			</div>
			<div class=""card-body"">
				");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "3efcb588353be60f891edbedc27c4d8280505a506356", async() => {
                WriteLiteral(@"
					<h6 class=""heading-small text-muted mb-4"">User information</h6>
					<input type=""hidden"" id=""inputHiddenUserId"">
					<div class=""pl-lg-4"">
						<div class=""row"">
							<div class=""col-lg-5"">
								<div class=""form-group"">
									<label class=""form-control-label"" for=""inputEmail"">Email (Username) <span style=""color:red"">*</span></label>
									<input type=""text"" id=""inputEmail"" class=""form-control form-control-alternative"" placeholder=""Email (Username)"">
								</div>
							</div>
							<div class=""col-lg-5"">
								<div class=""form-group"">
									<label class=""form-control-label"" for=""inputUserlevel"">Userlevel <span style=""color:red"">*</span></label>
									<select id=""inputUserlevel"" class=""form-control form-control-alternative"">
										");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "3efcb588353be60f891edbedc27c4d8280505a507443", async() => {
                    WriteLiteral("Select");
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral(@"
									</select>
								</div>
							</div>
						</div>
                        <div class=""row"">
							<div class=""col-lg-5"">
								<div class=""form-group"">
									<label class=""form-control-label"" for=""inputFirstName"">First Name <span style=""color:red"">*</span></label>
									<input type=""text"" id=""inputFirstName"" class=""form-control form-control-alternative"" placeholder=""First Name"">
								</div>
							</div>
                            <div class=""col-lg-5"">
								<div class=""form-group"">
									<label class=""form-control-label"" for=""inputLastName"">Last Name <span style=""color:red"">*</span></label>
									<input type=""text"" id=""inputLastName"" class=""form-control form-control-alternative"" placeholder=""Last Name"">
								</div>
							</div>
						</div>   
						<div class=""row"">
							<div class=""col-lg-5"">
								<div class=""form-group"">
									<label class=""form-control-label"" for=""inputPassword"">Password <span style=""color:red"">*</span></label>
								");
                WriteLiteral(@"	<input type=""password"" id=""inputPassword"" class=""form-control form-control-alternative"" placeholder=""Password"">
								</div>
							</div>
							<div class=""col-lg-5"">
								<div class=""form-group"">
									<label class=""form-control-label"" for=""inputConfirmPassword"">Confirm Password <span style=""color:red"">*</span></label>
									<input type=""password"" id=""inputConfirmPassword"" class=""form-control form-control-alternative"" placeholder=""Confirm Password"">
								</div>
							</div>
						</div>
						<div class=""row"">
							<div class=""col-lg-5"">
								<div class=""form-group"">
									<div class=""text-muted font-italic"">
										<small>password strength: <span id=""passwordLabelStrength"" class=""text-default font-weight-700"">Very Weak</span></small>
									</div>
								</div>
							</div>
							<div class=""col-lg-5"">
								<div class=""form-group"">
									<div class=""text-muted font-italic"">
										<small>password match: <span id=""passwordLabelMatch"" class=""text");
                WriteLiteral(@"-danger font-weight-700"">Not Match</span></small>
									</div>
								</div>
							</div>
						</div>
						<div class=""row"">
							<div class=""col-lg-5"">
								<div class=""form-group"">
									<label class=""form-control-label"" for=""inputDOB"">Date of birth </label>
									<input type=""date"" id=""inputDOB"" class=""form-control form-control-alternative"">
								</div>
							</div>
							<div class=""col-lg-5"">
								<div class=""form-group"">
									<label class=""form-control-label"" for=""inputPhone"">Phone No. </label>
									<input type=""text"" id=""inputPhone"" class=""form-control form-control-alternative"" placeholder=""Phone No."">
								</div>
							</div>
						</div> 
                        <div class=""row"">
							<div class=""col-lg-7"">
								<div class=""form-group"">
									<label class=""form-control-label"" for=""inputProfilePicture"">Profile Picture </label>
									<input type=""file"" id=""inputProfilePicture"" onchange=""setImage(this,'imgProfileImage')"" class=""form");
                WriteLiteral(@"-control form-control-alternative"" accept=""image/*""> 
								</div>
							</div>
							<div class=""col-lg-3"">
								<div class=""form-group"">
									<img id=""imgProfileImage"" class=""card-img-top"" src=""/img/noimage.png"" alt=""Card image cap"">
								</div> 
							</div>
						</div> 
                        <div class=""row"">
							<div class=""col-lg-5"">
								<div class=""form-group"">
									<label class=""form-control-label"" for=""inputDepartment"">Department <span style=""color:red"">*</span></label>
									<select id=""inputDepartment"" class=""form-control form-control-alternative"">
                                        ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "3efcb588353be60f891edbedc27c4d8280505a5012673", async() => {
                    WriteLiteral("Select");
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral(@"
                                    </select>
								</div>
							</div>
                            <div class=""col-lg-5"">
                                <div class=""form-group"">&nbsp;</div>
								<div class=""form-group"">
									<div class=""custom-control custom-control-alternative custom-checkbox mb-3"">
										<input class=""form-control custom-control-input"" id=""inputDepartmentHead"" type=""checkbox"">
										<label class=""form-control-label custom-control-label"" for=""inputDepartmentHead""> Is Department Head</label>
									</div>
								</div>
							</div>
						</div>
						<div class=""row"">
							<div class=""col-lg-6"">
								<div class=""form-group"">
									<div class=""custom-control custom-control-alternative custom-checkbox mb-3"">
										<input class=""custom-control-input"" id=""inputStatus"" type=""checkbox"" checked>
										<label class=""custom-control-label"" for=""inputStatus"">Status</label>
									</div>
								</div>
							</div>
						</div>
					</di");
                WriteLiteral("v>\r\n\t\t\t\t");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
				<hr class=""my-4"">
				<div class=""row"">
					<div class=""col-2"">
						<div class=""form-group focused"">
							<button type=""button"" class=""btn btn-success"" id=""btnSaveUser"">Save</button>
						</div>
					</div>
					<div class=""col-2"">
						<div class=""form-group focused"">
							<button type=""button"" class=""btn btn-danger"" id=""btnCancelUser"">Cancel</button>
						</div>
					</div>
					<div class=""col-lg-8 text-right"">
						<div class=""form-group focused"">
							<button type=""button"" class=""btn btn-default"" data-dismiss=""modal"">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "3efcb588353be60f891edbedc27c4d8280505a5016661", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "src", 2, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.SingleQuotes);
            AddHtmlAttributeValue("", 8369, "~/Script/user.js?rnd=", 8369, 21, true);
#nullable restore
#line 204 "C:\xampp\htdocs\DocRoute\Views\User\index.cshtml"
AddHtmlAttributeValue("", 8390, Guid.NewGuid(), 8390, 17, false);

#line default
#line hidden
#nullable disable
            EndAddHtmlAttributeValues(__tagHelperExecutionContext);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
